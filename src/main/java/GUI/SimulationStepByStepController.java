package GUI;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wargameclasses.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * The class which administrate result window after the step by step simulation
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class SimulationStepByStepController {
    private Army armyOneCopy = null; //copy of first army
    private Army armyTwoCopy = null; //copy of second army

    private Army winner = null; //winner of simulation

    public static Army winnerForResult = null; //winner of simulation for result window
    public static Army armyOneForResult = null; //army of simulation for result window to back with new data
    public static Army armyTwoForResult = null; //army of simulation for result window to back with new data

    private int i = 1; //step of simulation

    @FXML
    private Button nextStep; //button for next step

    @FXML
    private Text attackFirstUnit; //text with attack power of first unit

    @FXML
    private Text attackOrDefenseOne; //text to see who attack or defense on different languages

    @FXML
    private Text attackOrDefenseTwo; //text to see who attack or defense on different languages

    @FXML
    private Text attackSecondUnit; //text with attack power of second unit

    @FXML
    private Text defenseFirstUnit; //text with defense power of first unit

    @FXML
    private Text defenseSecondUnit; //text with defense power of second unit

    @FXML
    private Text hpFirstUnit; //text with health points of first unit

    @FXML
    private Text hpSecondUnit; //text with health points of second unit

    @FXML
    private ImageView imageOfFirstUnit; //image of first unit

    @FXML
    private ImageView imageOfSecondUnit; //image of second unit

    @FXML
    private Text nameOfArmy1; //name of first army

    @FXML
    private Text nameOfArmy2; //name of first army

    @FXML
    private ImageView swordBlue; //image to see if first army attacks

    @FXML
    private ImageView swordRed; //image to see if first army attacks

    @FXML
    private Text unitFirstArmy; // name of first unit

    @FXML
    private Text unitSecondArmy; //name of second unit

    @FXML
    private Button exitButton; // button to exit from window

    @FXML
    private Button toTheEndbutton; //to the end button

    @FXML
    private Text attackText; //text "attack power" for first unit on different languages

    @FXML
    private Text hpText; //text "hp" for first unit on different languages

    @FXML
    private Text defenseText; //text "defense power" for first unit on different languages

    @FXML
    private Text unitWithName; //text "Unit with name" on different languages

    @FXML
    private Label armyWithName; //text "Army with name:" on different languages

    @FXML
    private Text attackText2; //text "attack power" for second unit on different languages

    @FXML
    private Text hpText2; //text "hp" for second unit on different languages

    @FXML
    private Text defenseText2; //text "defense power" for second unit on different languages

    @FXML
    private Text unitWithName2; //text "Unit with name" on different languages

    @FXML
    private Label armyWithName2; //text "Army with name:" on different languages


    @FXML
    void initialize() {
        if (CreatingTheArmies.language.equals("ENG"))
            setEnglishLanguage();
        else if (CreatingTheArmies.language.equals("NOR"))
            setNorwegianLanguage();

        creatingOfCopies();

        nameOfArmy1.setText(armyOneCopy.getName());
        nameOfArmy2.setText(armyTwoCopy.getName());

        switch (MainWindowController.terrainType){
            case "NOTERRAIN":
                armyOneCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.NOTERRAIN));
                armyTwoCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.NOTERRAIN));
                break;
            case "HILL":
                armyOneCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.HILL));
                armyTwoCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.HILL));
                break;
            case "PLAINS":
                armyOneCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.PLAINS));
                armyTwoCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.PLAINS));
                break;
            case "FOREST":
                armyOneCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.FOREST));
                armyTwoCopy.getAllUnits().forEach(p->p.setTerrainType(TerrainType.FOREST));
                break;
        }
        SimulateStepByStep();

        nextStep.setOnAction(actionEvent -> {
            if (winner==null) {
                i++;
                SimulateStepByStep();
            }else {
                if (CreatingTheArmies.language.equals("ENG")) {
                    toTheEndbutton.setText("See results");
                    nextStep.setVisible(false);
                } else if (CreatingTheArmies.language.equals("NOR")) {
                    toTheEndbutton.setText("Se resultater");
                    nextStep.setVisible(false);
                }
            }
        });
        exitButton.setOnAction(actionEvent -> {
            Stage stage = (Stage) exitButton.getScene().getWindow();
            stage.close();
        });
        toTheEndbutton.setOnAction(actionEvent -> {
            if (winner==null) {
                while (winner == null) {
                    i++;
                    SimulateStepByStep();
                }
                if (CreatingTheArmies.language.equals("ENG")) {
                    toTheEndbutton.setText("See results");
                    nextStep.setVisible(false);
                } else if (CreatingTheArmies.language.equals("NOR")) {
                    toTheEndbutton.setText("Se resultater");
                    nextStep.setVisible(false);

                }
            }else {
                winnerForResult=winner;
                armyOneForResult = armyOneCopy;
                armyTwoForResult = armyTwoCopy;
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("/GUI/resultForStepByStepSimulation.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage window = (Stage) toTheEndbutton.getScene().getWindow();
                window.getIcons().add(new Image("images/icon.png"));
                window.setTitle("WarGame Application");
                window.setScene(new Scene(root));



            }
            });
        }

    /**
     * Method which creates copies of armies
     * before simulation
     * We need it to have possibility to return with old data
     */
    void creatingOfCopies(){
        ArrayList<Unit> listOfArmyOne = new ArrayList<>();
        for (Unit unit: CreatingTheArmies.armyOne.getInfantryUnits()){
            unit = new InfantryUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getRangedUnits()){
            unit = new RangedUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getCavalryUnits()){
            unit = new CavalryUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getCommanderUnits()){
            unit = new CommanderUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getMagicUnits()){
            unit = new MagicUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }

        ArrayList<Unit> listOfArmyTwo = new ArrayList<>();
        for (Unit unit: CreatingTheArmies.armyTwo.getInfantryUnits()){
            unit = new InfantryUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getRangedUnits()){
            unit = new RangedUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getCavalryUnits()){
            unit = new CavalryUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getCommanderUnits()){
            unit = new CommanderUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getMagicUnits()){
            unit = new MagicUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        armyOneCopy = new Army(CreatingTheArmies.armyOne.getName(),listOfArmyOne);
        armyTwoCopy = new Army(CreatingTheArmies.armyTwo.getName(),listOfArmyTwo);
    }

    private void SimulateStepByStep(){
        if (armyOneCopy.hasUnits() && !armyTwoCopy.hasUnits())
            winner = armyOneCopy;
        else if (!armyOneCopy.hasUnits() && armyTwoCopy.hasUnits())
            winner = armyTwoCopy;
        else {
            if (i%2==1){
                swordBlue.setVisible(true);
                swordRed.setVisible(false);

                Unit attacker = armyOneCopy.getRandom();
                Unit defender = armyTwoCopy.getRandom();

                if (CreatingTheArmies.language.equals("ENG")) {
                    attackOrDefenseOne.setText("attacks");
                    attackOrDefenseTwo.setText("defense");
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    attackOrDefenseOne.setText("angrep");
                    attackOrDefenseTwo.setText("forsvar");
                }
                attackFirstUnit.setText(String.valueOf(attacker.getAttack()+attacker.getAttackBonus()));
                attackSecondUnit.setText(String.valueOf(defender.getAttack()+defender.getAttackBonus()));
                unitFirstArmy.setText(attacker.getName());
                unitSecondArmy.setText(defender.getName());
                hpFirstUnit.setText(String.valueOf(attacker.getHealth()));
                hpSecondUnit.setText(String.valueOf(defender.getHealth()));
                defenseFirstUnit.setText(String.valueOf(attacker.getArmor()+ attacker.getResistBonus()));
                defenseSecondUnit.setText(String.valueOf(defender.getArmor()+ defender.getResistBonus()));

                if (attacker instanceof InfantryUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/knight1.png")));
                else if (attacker instanceof RangedUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/archer1.png")));
                else if (attacker instanceof CommanderUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/commander1.png")));
                else if (attacker instanceof MagicUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/wizard.png")));
                else
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/Cavalry1.png")));

                if (defender instanceof InfantryUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/knight2.png")));
                else if (defender instanceof RangedUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/archer2.png")));
                else if (defender instanceof CommanderUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/commander2.png")));
                else if (defender instanceof MagicUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/wizard2.png")));
                else
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/Cavalry2.png")));

                attacker.attack(defender);
                if (defender.getHealth()<=0) armyTwoCopy.remove(defender);
            }

            else if (i%2==0){
                swordBlue.setVisible(false);
                swordRed.setVisible(true);

                Unit attacker = armyTwoCopy.getRandom();
                Unit defender = armyOneCopy.getRandom();

                if (CreatingTheArmies.language.equals("ENG")) {
                    attackOrDefenseOne.setText("defense");
                    attackOrDefenseTwo.setText("attacks");
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    attackOrDefenseOne.setText("forsvar");
                    attackOrDefenseTwo.setText("angrep");
                }

                attackFirstUnit.setText(String.valueOf(defender.getAttack()+defender.getAttackBonus()));
                attackSecondUnit.setText(String.valueOf(attacker.getAttack()+attacker.getAttackBonus()));
                unitFirstArmy.setText(defender.getName());
                unitSecondArmy.setText(attacker.getName());
                hpFirstUnit.setText(String.valueOf(defender.getHealth()));
                hpSecondUnit.setText(String.valueOf(attacker.getHealth()));
                defenseFirstUnit.setText(String.valueOf(defender.getArmor()+ defender.getResistBonus()));
                defenseSecondUnit.setText(String.valueOf(attacker.getArmor()+ attacker.getResistBonus()));

                if (defender instanceof InfantryUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/knight1.png")));
                else if (defender instanceof RangedUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/archer1.png")));
                else if (defender instanceof CommanderUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/commander1.png")));
                else if (defender instanceof MagicUnit)
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/wizard.png")));
                else
                    imageOfFirstUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/Cavalry1.png")));

                if (attacker instanceof InfantryUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/knight2.png")));
                else if (attacker instanceof RangedUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/archer2.png")));
                else if (attacker instanceof CommanderUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/commander2.png")));
                else if (attacker instanceof MagicUnit)
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/wizard2.png")));
                else
                    imageOfSecondUnit.setImage(new Image(SimulationStepByStepController.class.getResourceAsStream("/images/Cavalry2.png")));

                attacker.attack(defender);
                if (defender.getHealth()<=0) armyOneCopy.remove(defender);
            }
        }
    }
    private void setEnglishLanguage(){
        armyWithName.setText("Army with name:");
        unitWithName.setText("Unit with name");
        hpText.setText("hp:");
        attackText.setText("attack power:");
        defenseText.setText("defense power:");

        armyWithName2.setText("Army with name:");
        unitWithName2.setText("Unit with name");
        hpText2.setText("hp:");
        attackText2.setText("attack power:");
        defenseText2.setText("defense power:");

        nextStep.setText("Next step");
        toTheEndbutton.setText("To the end");
        exitButton.setText("Exit");
    }

    private void setNorwegianLanguage(){
        armyWithName.setText("Hæren med navn:");
        unitWithName.setText("Enhet med navn");
        hpText.setText("hp:");
        attackText.setText("angrepsstyrke:");
        defenseText.setText("forsvarsmakt:");
        armyWithName2.setText("Hæren med navn:");
        unitWithName2.setText("Enhet med navn");
        hpText2.setText("hp:");
        attackText2.setText("angrepsstyrke:");
        defenseText2.setText("forsvarsmakt:");
        nextStep.setText("Neste steg");
        toTheEndbutton.setText("Til slutten");
        exitButton.setText("Utgang");
    }


}
