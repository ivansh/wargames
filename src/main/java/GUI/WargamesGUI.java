package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main application class
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class WargamesGUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/CreatingTheArmies.fxml"));
        Scene scene = new Scene(root);

        primaryStage.getIcons().add(new Image("images/icon.png"));
        primaryStage.setTitle("WarGame Application");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();




    }
}
