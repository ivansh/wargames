package GUI;

import javafx.animation.*;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import wargameclasses.*;

import java.io.File;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.Glow;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Window in which user create two armies (manual with help of name)
 * or with import of csv file
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class CreatingTheArmies {

    public static String language = "ENG";  // select the language in application is available in first window and in the main window of application

    public static Army armyOne = null;      // first army in application is not initialised yet
    public static Army armyTwo = null;      // second army in application is not initialised yet

    @FXML
    private Text OneIsInitialisated;        // text appears if army one is initialised

    @FXML
    private Text TwoIsInitialisated;        //text appears if army one is initialised

    @FXML
    private Button clearArmyOne;            //button for clear army one if it was import wrong

    @FXML
    private Button clearArmyTwo;            //button for clear army two if it was import wrong

    @FXML
    private Label orOne;                    //label for word "or" under first army

    @FXML
    private Label orTwo;                    //label for word "or" under second army

    @FXML
    private Label informationOneLabel;      //useful information first string

    @FXML
    private Label informationTwoLabel;      //useful information second string

    @FXML
    private ChoiceBox<String> choiceLanguage;   //choose language

    @FXML
    private Button continueButton;  //continue button after armies is initialised

    @FXML
    private Button exit;    //exit from application

    @FXML
    private Button importArmyOneFromCsv; //import first army from csv button

    @FXML
    private Button importArmyTwoFromCsv; //import second army from csv button

    @FXML
    private TextField nameOfFirstArmy; //name for first army if we initialise it manual

    @FXML
    private TextField nameOfSecondArmy; //name for second army if we initialise it manual

    @FXML
    private ImageView swordBlue;    //1st picture for beautiful animation

    @FXML
    private ImageView swordRed;     //2nd picture for beautiful animation

    @FXML
    private Label welcomeText;      //welcome text

    @FXML
    private ImageView whiteSword;   //3rd picture for beautiful animation

    /**
     * Method execute then we open this window
     */
    @FXML
    void initialize() {
        choiceLanguage.setItems(FXCollections.observableArrayList(
                "ENG", "NOR"));
        choiceLanguage.getSelectionModel().select(0);

        choiceLanguage.setOnHiding(actionEvent -> {
            if (choiceLanguage.getSelectionModel().getSelectedItem().equals("ENG"))
                setEnglishLanguage();
            else if (choiceLanguage.getSelectionModel().getSelectedItem().equals("NOR"))
                setNorwegianLanguage();
        });

        OneIsInitialisated.setVisible(false);
        TwoIsInitialisated.setVisible(false);
        clearArmyOne.setVisible(false);
        clearArmyTwo.setVisible(false);

        welcomeText.setEffect(new Glow(0.9));
        animationsForSwords(2);


        final String[] pathToArmyOne = {null};
        final String[] pathToArmyTwo = {null};
        importArmyOneFromCsv.setOnAction(actionEvent -> {
            pathToArmyOne[0] =fileChooser();
            if (pathToArmyOne[0]!=null) {
                try {
                    armyOne = Army.readCsvFile(pathToArmyOne[0]);
                    nameOfFirstArmy.clear();
                    nameOfFirstArmy.setVisible(false);
                    OneIsInitialisated.setVisible(true);
                    clearArmyOne.setVisible(true);
                } catch (Exception e) {
                    CustomDialog dialog = new CustomDialog(e.getMessage());
                    dialog.openDialog();
                }
            }else {
                String message;
                if (language.equals("ENG")) message = "File wasn't chosen";
                else message = "Filen ble ikke valgt";
                CustomDialog dialog = new CustomDialog(message);
                dialog.openDialog();
            }
        });
        clearArmyOne.setOnAction(actionEvent1 -> {
            armyOne = null;
            OneIsInitialisated.setVisible(false);
            clearArmyOne.setVisible(false);
            nameOfFirstArmy.setVisible(true);
        });
        importArmyTwoFromCsv.setOnAction(actionEvent -> {
            pathToArmyTwo[0] =fileChooser();
            if (pathToArmyTwo[0]!=null) {
                try {
                    armyTwo = Army.readCsvFile(pathToArmyTwo[0]);
                    nameOfSecondArmy.clear();
                    nameOfSecondArmy.setVisible(false);
                    TwoIsInitialisated.setVisible(true);
                    clearArmyTwo.setVisible(true);
                } catch (Exception e) {
                    CustomDialog dialog = new CustomDialog(e.getMessage());
                    dialog.openDialog();
                }
            }else {
                String message;
                if (language.equals("ENG")) message = "File wasn't chosen";
                else message = "Filen ble ikke valgt";
                CustomDialog dialog = new CustomDialog(message);
                dialog.openDialog();
            }
        });
        clearArmyTwo.setOnAction(actionEvent1 -> {
            armyTwo = null;
            TwoIsInitialisated.setVisible(false);
            clearArmyTwo.setVisible(false);
            nameOfSecondArmy.setVisible(true);
        });
        exit.setOnAction(actionEvent -> System.exit(0));
        continueButton.setOnAction(actionEvent -> {

            try {
                if (!nameOfFirstArmy.getText().isEmpty()) armyOne = new Army(nameOfFirstArmy.getText());
                if (!nameOfSecondArmy.getText().isEmpty()) armyTwo = new Army(nameOfSecondArmy.getText());
                if (armyOne !=null && armyTwo!=null) switchToMainMenuScene();
                else {
                    CustomDialog customDialog = new CustomDialog("Army One or Army Two is empty");
                    customDialog.openDialog();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        nameOfFirstArmy.setStyle("-fx-text-fill: #c3073f;-fx-background-color: #1A1A1D;-fx-border-color: #c3073f;-fx-border-radius:5");
        nameOfSecondArmy.setStyle("-fx-text-fill: #c3073f;-fx-background-color: #1A1A1D;-fx-border-color: #c3073f;-fx-border-radius:5");



    }


    /**
     * switching to main menu scene
     * @throws IOException if getResource can`t find the path to fxml-file
     */
    public void switchToMainMenuScene() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/MainWindow.fxml"));
        Stage window = (Stage) continueButton.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Animation for swords on the screen
     * @param seconds animation played
     */
    public void animationsForSwords(int seconds){
        RotateTransition rotateOfBlueSword = new RotateTransition(Duration.seconds(seconds),swordBlue);
        rotateOfBlueSword.setToAngle(360);
        rotateOfBlueSword.play();

        TranslateTransition blue = new TranslateTransition(Duration.seconds(seconds), swordBlue);
        blue.setByY(-80);
        blue.setByX(180);
        blue.play();

        RotateTransition rotateOfRed = new RotateTransition(Duration.seconds(seconds),swordRed);

        rotateOfRed.setToAngle(360);
        rotateOfRed.play();

        TranslateTransition red = new TranslateTransition(Duration.seconds(seconds), swordRed);
        red.setByY(-80);
        red.setByX(-185);
        red.play();

        TranslateTransition whiteDrop = new TranslateTransition(Duration.seconds(seconds), whiteSword);
        whiteDrop.setByY(245);
        whiteDrop.setByX(0);
        whiteDrop.play();
    }

    /**
     * File chooser which gives us a path to file
     * @return path to file
     */
    private String fileChooser(){
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(".csv FILES","*.csv"));
        File file =fc.showOpenDialog(null);
        if (file!=null){
            return file.getPath();
        }return null;
    }

    /**
     * Set english language in our application
     */
    private void setEnglishLanguage(){
        orOne.setText("or");
        orTwo.setText("or");
        welcomeText.setText("Welcome to the War Game!");
        informationOneLabel.setText("Here you can simulate battle between two armies");
        informationTwoLabel.setText("Enter names or import armies form .csv format");
        nameOfFirstArmy.setPromptText("Enter the name");
        nameOfSecondArmy.setPromptText("Enter the name ");
        importArmyOneFromCsv.setText("Import army from .csv");
        importArmyTwoFromCsv.setText("Import army from .csv");
        continueButton.setText("Continue");
        exit.setText("Exit");
        OneIsInitialisated.setText("Is initialised");
        TwoIsInitialisated.setText("Is initialised");
        clearArmyOne.setText("Clear");
        clearArmyTwo.setText("Clear");

        language="ENG";
    }

    /**
     * Set english language in our application
     */
    private void setNorwegianLanguage(){
        orOne.setText("eller");
        orTwo.setText("eller");
        welcomeText.setText("Velkommen til War Game!");
        informationOneLabel.setText("Her kan du simulere kamp mellom to hærer.");
        informationTwoLabel.setText("Skriv inn navn eller importer hærer fra .csv-filen");
        nameOfFirstArmy.setPromptText("Skriv inn navnet");
        nameOfSecondArmy.setPromptText("Skriv inn navnet");
        importArmyOneFromCsv.setText("Importer hær fra .csv");
        importArmyTwoFromCsv.setText("Importer hær fra .csv");
        continueButton.setText("Fortsette");
        exit.setText("Utgang");
        OneIsInitialisated.setText("Er initialisert");
        TwoIsInitialisated.setText("Er initialisert");
        clearArmyOne.setText("Tøm");
        clearArmyTwo.setText("Tøm");

        language="NOR";
    }
}

