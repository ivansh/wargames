package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wargameclasses.Army;
import wargameclasses.Unit;

/**
 * Class of window which show us full information about army
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class InfoAboutArmyController {

    public static Army armyInfo = null; /* system army which replaces itself with army one or army two
                                                                when we use button in main window */

    @FXML
    private Label armyWithName; //label with name of army

    @FXML
    private Text hasUnits; //text "has units:" on different languages

    @FXML
    private Text infoDoubleClick; //useful information about how to delete object from army on different languages

    @FXML
    private TableColumn<Unit, Integer> attackCol = new TableColumn<>(); //column with attack power of units

    @FXML
    private TableColumn<Unit, Integer> defCol = new TableColumn<>(); //column with defense power of units

    @FXML
    private TableColumn<Unit, Integer> hpOfUnitCol = new TableColumn<>(); //column with health point of units

    @FXML
    private Text nameOfArmy; //text with name of army about which we get information

    @FXML
    private TableColumn<Unit, String> nameOfUnitCol = new TableColumn<>(); //column with names of units

    @FXML
    private Button okButton = new Button(); //ok button when user saw all information and made changes if it needed

    @FXML
    private TableView<Unit> tableView; //table with information about army

    /**
     * Method execute then we open this window
     */
    @FXML
    void initialize() {
        if (CreatingTheArmies.language.equals("ENG"))
            setEnglishLanguage();
        else if (CreatingTheArmies.language.equals("NOR"))
            setNorwegianLanguage();
        nameOfArmy.setText(armyInfo.getName());
        buildTableView();
        okButton.setOnAction(actionEvent -> {
            Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
        });

    }

    /**
     * Build tableView with information in window
     * in double-click we can delete unit
     */
    void buildTableView(){
        nameOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,String>("name"));
        hpOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("health"));
        attackCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("attack"));
        defCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("armor"));
        ObservableList<Unit> unitList = FXCollections.observableArrayList();
        unitList.addAll(armyInfo.getAllUnits());
        tableView.setItems(unitList);

        tableView.setRowFactory( tv -> {
            TableRow<Unit> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Unit rowData = row.getItem();
                    armyInfo.remove(rowData);
                    int selectedUnitId = tableView.getSelectionModel().getSelectedIndex();
                    tableView.getItems().remove(selectedUnitId);
                }
            });
            return row ;
        });
    }
    /**
     * Set english language in window
     */
    private void setEnglishLanguage(){
        armyWithName.setText("Army with name: ");
        hasUnits.setText("has units:");
        infoDoubleClick.setText("You can remove unit on double click");
        nameOfUnitCol.setText("Name");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Attack");
        defCol.setText("Defense");
    }
    /**
     * Set norwegian language in window
     */
    private void setNorwegianLanguage(){
        armyWithName.setText("Hæren med navn: ");
        hasUnits.setText("har enheter: ");
        infoDoubleClick.setText("Du kan fjerne enheten ved å dobbeltklikke");
        nameOfUnitCol.setText("Navn");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Angrep");
        defCol.setText("Forsvar");
    }
}
