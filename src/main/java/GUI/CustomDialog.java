package GUI;

import javafx.animation.Interpolator;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * Animation for errors and Custom Dialog
 * Animation was made according tutorial by Almas Baimagambetov
 * https://www.youtube.com/watch?v=vrEnmDZW7Ag
 * but styled according my design
 * @author Kandidatnr:10049
 * @version 1.0.0
 */

public class CustomDialog extends Stage {

    private static final Interpolator EXP_IN = new Interpolator() {
        @Override
        protected double curve(double t) {
            return (t == 1.0) ? 1.0 : 1 - Math.pow(2.0, -10 * t);
        }
    };

    private static final Interpolator EXP_OUT = new Interpolator() {
        @Override
        protected double curve(double t) {
            return (t == 0.0) ? 0.0 : Math.pow(2.0, 10 * (t - 1));
        }
    };

    private ScaleTransition scale1 = new ScaleTransition();
    private ScaleTransition scale2 = new ScaleTransition();

    private SequentialTransition anim = new SequentialTransition(scale1, scale2);

    /**
     * Constructor for Custom dialog
     * @param content string with information which we will get on screen
     */
    public CustomDialog(String content){
        Pane root = new Pane();


        scale1.setFromX(0.01);
        scale1.setFromY(0.01);
        scale1.setToY(1.0);
        scale1.setDuration(Duration.seconds(0.33));
        scale1.setInterpolator(EXP_IN);
        scale1.setNode(root);

        scale2.setFromX(0.01);
        scale2.setToX(1.0);
        scale2.setDuration(Duration.seconds(0.33));
        scale2.setInterpolator(EXP_OUT);
        scale2.setNode(root);

        initStyle(StageStyle.TRANSPARENT);
        initModality(Modality.APPLICATION_MODAL);

        Rectangle bg = new Rectangle(300, 100, Color.valueOf("#1A1A1D"));


        Text headerText = new Text();
        if (CreatingTheArmies.language.equals("ENG"))
            headerText.setText ("Something went wrong");
        else if (CreatingTheArmies.language.equals("NOR"))
            headerText.setText ("Noe gikk galt");

        headerText.setFill(Color.valueOf("#c3073f"));
        headerText.setFont(Font.font("MS Gothic", FontWeight.BOLD, FontPosture.REGULAR, 20));


        Text contentText = new Text(content);
        contentText.setFill(Color.valueOf("#c3073f"));
        contentText.setFont(Font.font("MS Gothic", FontWeight.NORMAL, FontPosture.REGULAR, 16));


        VBox box = new VBox(headerText,
                new Separator(Orientation.HORIZONTAL),
                contentText
        );



        Button btn = new Button("OK");
        btn.setTranslateX(bg.getWidth() - 50);
        btn.setTranslateY(bg.getHeight() - 50);
        btn.setOnAction(e -> closeDialog());
        btn.getStylesheets().add("/styles/styleForButton.css");





        root.getChildren().addAll(bg,box,btn);

        setScene(new Scene(root, null));
    }

    /**
     * Method for opening the Custom Dialog
     */
    void openDialog() {
        show();

        anim.play();
    }

    /**
     * Close Custom Dialog with animation
     */
    void closeDialog() {
        anim.setOnFinished(e -> close());
        anim.setAutoReverse(true);
        anim.setCycleCount(2);
        anim.playFrom(Duration.seconds(0.66));
    }

    }


