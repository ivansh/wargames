package GUI;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wargameclasses.*;

/**
 * Window for adding units in our armies
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class AddUnitsController {
    public static Army addUnitsArmy = new Army("System"); /* system army which replaces itself with army one or army two
                                                                when we use button in main window */

    @FXML
    private Text infoText; //text with useful information about input of data

    @FXML
    private Button cancelButton= new Button();  // button for canceling adding units

    @FXML
    private TextField hpTextField; //text input for hp of unit/units

    @FXML
    private TextField nameTextField; //text input for name of unit/units

    @FXML
    private TextField numberTextField; // how many units we will add

    @FXML
    private Button okButton; //confirmation button

    @FXML
    private ChoiceBox<String> typeChoice; //choose what type of units will we add

    /**
     * Method execute then we open this window
     */
    @FXML
    void initialize() {
        if (CreatingTheArmies.language.equals("ENG"))
            setEnglishLanguage();
        else if (CreatingTheArmies.language.equals("NOR"))
            setNorwegianLanguage();
        cancelButton.setOnAction(actionEvent -> {
            Stage stage = (Stage) cancelButton.getScene().getWindow();
            stage.close();
        });
        typeChoice.setItems(FXCollections.observableArrayList(
                "INFANTRY", "RANGED",
                 "CAVALRY", "COMMANDER", "WIZARD"));

        integersOnly();
        styles();



        okButton.setOnAction(actionEvent -> {
            if (isValid()) {
                try {
                    for (int i = 0; i < Integer.parseInt(numberTextField.getText()); i++) {
                        switch (typeChoice.getSelectionModel().getSelectedItem()) {
                            case "INFANTRY":
                                addUnitsArmy.add(new InfantryUnit(nameTextField.getText(), Integer.parseInt(hpTextField.getText())));
                                break;
                            case "RANGED":
                                addUnitsArmy.add(new RangedUnit(nameTextField.getText(), Integer.parseInt(hpTextField.getText())));
                                break;
                            case "CAVALRY":
                                addUnitsArmy.add(new CavalryUnit(nameTextField.getText(), Integer.parseInt(hpTextField.getText())));
                                break;
                            case "COMMANDER":
                                addUnitsArmy.add(new CommanderUnit(nameTextField.getText(), Integer.parseInt(hpTextField.getText())));
                                break;
                            case "WIZARD":
                                addUnitsArmy.add(new MagicUnit(nameTextField.getText(), Integer.parseInt(hpTextField.getText())));
                                break;
                        }
                    }
                    Stage stage = (Stage) okButton.getScene().getWindow();
                    stage.close();
                }catch (IllegalArgumentException exception){
                    CustomDialog customDialog = new CustomDialog(exception.getMessage());
                    customDialog.openDialog();
                }
            }


        });
    }
    /**
     * Method for user can input only integers in text fields
     * with hp and number of units
     * This will help user to use the app and avoid the exception
     */
    private void integersOnly(){
        hpTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    hpTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        numberTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    numberTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

    /**
     * Method for stylising
     * the text fields
     */
    private void styles(){
        hpTextField.setStyle("-fx-text-fill: #c3073f;-fx-background-color: #1A1A1D;-fx-border-color: #c3073f;-fx-border-radius:5");
        numberTextField.setStyle("-fx-text-fill: #c3073f;-fx-background-color: #1A1A1D;-fx-border-color: #c3073f;-fx-border-radius:5");
        nameTextField.setStyle("-fx-text-fill: #c3073f;-fx-background-color: #1A1A1D;-fx-border-color: #c3073f;-fx-border-radius:5");
    }

    /**
     * Method which check if user`s data is valid
     * @return true if it is valid, false if it`s invalid
     */
    private boolean isValid(){
        if (numberTextField.getText().isEmpty() || Integer.parseInt(numberTextField.getText())<=0){
            CustomDialog customDialog = new CustomDialog("Number should be more than 0");
            customDialog.openDialog();
            return false;
        }
        else if (hpTextField.getText().isEmpty()){
            CustomDialog customDialog = new CustomDialog("Health point field is empty");
            customDialog.openDialog();
            return false;
        }
        else if (typeChoice.getSelectionModel().getSelectedItem()==null){
            CustomDialog customDialog = new CustomDialog("Type of unit was not chosen");
            customDialog.openDialog();
            return false;
        }
        else if (nameTextField.getText().isEmpty()) {
            CustomDialog customDialog = new CustomDialog("Name was not given to unit ");
            customDialog.openDialog();
            return false;
        }
        else return true;
    }

    /**
     * Set english language in window
     */
    private void setEnglishLanguage(){
        infoText.setText("only numbers in next fields*");
        nameTextField.setPromptText("Enter the name");
        hpTextField.setPromptText("Enter the hp");
        numberTextField.setPromptText("Enter the number of Units");
        cancelButton.setText("Cancel");
    }

    /**
     * Set norwegian language in window
     */
    private void setNorwegianLanguage(){
        infoText.setText("bare tall i neste felter");
        nameTextField.setPromptText("Skriv inn navnet");
        hpTextField.setPromptText("Skriv inn hp");
        numberTextField.setPromptText("Angi antall enheter");
        cancelButton.setText("Avbryt");
    }



}
