package GUI;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wargameclasses.Army;
import wargameclasses.Unit;

/**
 * Class of window which show us certain units info with clicking on picture
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class GetCertainUnitController {
    public static Army armyInfo = new Army("System"); /* system army which replaces itself with army one or army two
                                                                when we use button in main window */
    public static String type = null; // type of unit about which we will receive information

    @FXML
    private Label armyWithNamelabel; // label with text "Army with name:" on different languages

    @FXML
    private Text hasLabel; // text with word "has" on different languages

    @FXML
    private Text infoLabel; // label with information about deleting the units from collection

    @FXML
    private Text unitsLabel; // text with text " units:" on different languages

    @FXML
    private TableColumn<Unit, Integer> attackCol; //column with attack of unit

    @FXML
    private TableColumn<Unit, Integer> defCol; //column with defense power of unit

    @FXML
    private TableColumn<Unit, Integer> hpOfUnitCol; //column with health points of unit

    @FXML
    private Text nameOfArmy; //text with name of unit

    @FXML
    private TableColumn<Unit, String> nameOfUnitCol; //column with name of unit

    @FXML
    private Button okButton; //ok button when user saw all information and made changes if it needed

    @FXML
    private TableView<Unit> tableView; //table with information about certain unit in army

    @FXML
    private Text typeOfUnit; //text with type of unit about which we get information

    /**
     * Method execute then we open this window
     */
    @FXML
    void initialize() {
        buildTableView();
        typeOfUnit.setText(type);
        nameOfArmy.setText(armyInfo.getName() + " ");
        okButton.setOnAction(actionEvent -> {
            Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
        });

        if (CreatingTheArmies.language.equals("ENG"))
            setEnglishLanguage();
        else if (CreatingTheArmies.language.equals("NOR"))
            setNorwegianLanguage();
    }

    /**
     * Method which build table view with data about certain unit
     * In double-click we delete unit
     */
    void buildTableView(){
        nameOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,String>("name"));
        hpOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("health"));
        attackCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("attack"));
        defCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("armor"));
        ObservableList<Unit> unitList = FXCollections.observableArrayList();
        if (type.equals("INF")) unitList.addAll(armyInfo.getInfantryUnits());
        else if (type.equals("RAN")) unitList.addAll(armyInfo.getRangedUnits());
        else if (type.equals("CAV")) unitList.addAll(armyInfo.getCavalryUnits());
        else if (type.equals("COMM")) unitList.addAll(armyInfo.getCommanderUnits());
        else if (type.equals("MAG")) unitList.addAll(armyInfo.getMagicUnits());


        tableView.setItems(unitList);

        tableView.setRowFactory( tv -> {
            TableRow<Unit> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Unit rowData = row.getItem();
                    armyInfo.remove(rowData);
                    int selectedUnitId = tableView.getSelectionModel().getSelectedIndex();
                    tableView.getItems().remove(selectedUnitId);
                }
            });
            return row ;
        });
    }

    /**
     * Set english language in window
     */
    private void setEnglishLanguage(){
        armyWithNamelabel.setText("Army with name:");
        hasLabel.setText("has ");
        unitsLabel.setText(" units:");
        infoLabel.setText("You can remove unit on double click");
        nameOfUnitCol.setText("Name");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Attack");
        defCol.setText("Defense");
    }
    /**
     * Set norwegian language in window
     */
    private void setNorwegianLanguage(){
        armyWithNamelabel.setText("Hæren med navn:");
        hasLabel.setText("har ");
        unitsLabel.setText(" enheter:");
        infoLabel.setText("Du kan fjerne enheten ved å dobbeltklikke");
        nameOfUnitCol.setText("Navn");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Angrep");
        defCol.setText("Forsvar");
    }

}
