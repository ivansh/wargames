package GUI;

import java.io.File;
import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import static GUI.CreatingTheArmies.language;

/**
 * Class which administrate Main Window of my game
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class MainWindowController {
    public static String terrainType = "NOTERRAIN"; //terrain in battle

    private int versionOfExportFirst = 0;   //variable for export first
                                            // army with version (for not to accidentally replace the old army with a new one)
    private int versionOfExportSecond = 0;  //variable for export second
                                            // army with version (for not to accidentally replace the old army with a new one)

    @FXML
    private Button exportFirstArmy; //button export first army

    @FXML
    private Button exportSecondArmy; //button export second army

    @FXML
    private Button exitButton; //button to exit form application

    @FXML
    private Button backToCreatingButton; //create new armies move to CreatingTheArmies controller

    @FXML
    private Button getWizardOne; //button to get wizards from first army

    @FXML
    private Button getWizardsTwo; //button to get wizards from second army

    @FXML
    private Text numberWizardsOne; //text with number of wizards in first army

    @FXML
    private Text numberWizardsTwo; //text with number of wizards in second army

    @FXML
    private ChoiceBox<String> chooseLanguage; //choice box with languages for our application

    @FXML
    private Label armyOneWithName; //label with text "Army One with name: " on different languages

    @FXML
    private Label armyTwoWithName; //label with text "Army Two with name: " on different languages

    @FXML
    private Text chooseTerrainLabel; // text "Choose terrain" on different languages

    @FXML
    private Text hasUnitsOne; //text "has units:" on different languages

    @FXML
    private Text hasUnitsTwo; //text "has units:" on different languages

    @FXML
    private Text infoLabel; //text with useful information

    @FXML
    private Button addUnitsArmyOne; //button to add units in first army

    @FXML
    private Button addUnitsArmyTwo; //button to add units in first army

    @FXML
    private Button getCavArmyOne; //button to get cavalry units in first army

    @FXML
    private Button getCavArmyTwo; //button to get cavalry units in second army

    @FXML
    private Button getCommArmyOne; //button to get commander units in first army

    @FXML
    private Button getCommArmyTwo; //button to get commander units in second army

    @FXML
    private Button getInfArmyOne; //button to get infantry units in first army

    @FXML
    private Button getInfArmyTwo; //button to get infantry units in second army

    @FXML
    private Button getInfoAboutFirstArmy; //button to get all units in first army

    @FXML
    private Button getInfoAboutSecondArmy; //button to get all units in second army

    @FXML
    private Button getRanArmyOne; //button to get ranged units in first army

    @FXML
    private Button getRanArmyTwo; //button to get ranged units in second army

    @FXML
    private Text nameOfArmyOne; // text with name of first army

    @FXML
    private Text nameOfArmyTwo; // text with name of second army

    @FXML
    private ChoiceBox<String> choiceTerrain = new ChoiceBox<>(); //choice box for terrain

    @FXML
    private Text numberCavalryOne; //text with number of cavalry units in first army

    @FXML
    private Text numberCavalryTwo; //text with number of cavalry units in second army

    @FXML
    private Text numberCommanderOne; //text with number of commander units in first army

    @FXML
    private Text numberCommanderTwo; //text with number of commander units in second army

    @FXML
    private Text numberInfantryOne; //text with number of infantry units in first army

    @FXML
    private Text numberInfantryTwo; //text with number of infantry units in second army

    @FXML
    private Text numberRangedOne; //text with number of ranged units in first army

    @FXML
    private Text numberRangedTwo; //text with number of ranged units in second army

    @FXML
    private Button simulateButton; //button for usual simulation

    @FXML
    private Button simulateStepByStepButton; //button for simualtion step by step

    /**
     * Method execute then we open this window
     */
    @FXML
    void initialize() {

        chooseLanguage.setItems(FXCollections.observableArrayList(
                "ENG", "NOR"));

        if (CreatingTheArmies.language.equals("ENG")) {
            setEnglishLanguage();
            chooseLanguage.getSelectionModel().select(0);
        }
        else if (CreatingTheArmies.language.equals("NOR")) {
            setNorwegianLanguage();
            chooseLanguage.getSelectionModel().select(1);
        }


        chooseLanguage.setOnHiding(actionEvent -> {
            if (chooseLanguage.getSelectionModel().getSelectedItem().equals("ENG"))
                setEnglishLanguage();
            else if (chooseLanguage.getSelectionModel().getSelectedItem().equals("NOR"))
                setNorwegianLanguage();
        });


        textsForNames();
        setNumberUnits();
        getCertainUnit();

        choiceTerrain.setItems(FXCollections.observableArrayList(
                "NOTERRAIN", "HILL",
                "PLAINS", "FOREST"));
        choiceTerrain.getSelectionModel().select(0);



        simulateButton.setOnAction(actionEvent -> {
            if (!CreatingTheArmies.armyOne.hasUnits() && !CreatingTheArmies.armyTwo.hasUnits()) {
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Armies are empty");
                    customDialog.openDialog();
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hærene er tomme");
                    customDialog.openDialog();
                }
            }
            else if (!CreatingTheArmies.armyOne.hasUnits() && CreatingTheArmies.armyTwo.hasUnits()) {
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Army one is empty");
                    customDialog.openDialog();
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hær en er tom");
                    customDialog.openDialog();
                }
            }
            else if (CreatingTheArmies.armyOne.hasUnits() && !CreatingTheArmies.armyTwo.hasUnits()) {
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Army two is empty");
                    customDialog.openDialog();
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hær to er tom");
                    customDialog.openDialog();
                }
            }
            else if (CreatingTheArmies.armyOne.equals(CreatingTheArmies.armyTwo)) {
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Armies are equal");
                    customDialog.openDialog();
                } else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hærene er like");
                    customDialog.openDialog();
                }
            }
            else {
                terrainType = choiceTerrain.getSelectionModel().getSelectedItem();
                moveTo("/GUI/result.fxml");
                }
        });

        addUnitsArmyOne.setOnAction(actionEvent -> {
            AddUnitsController.addUnitsArmy = CreatingTheArmies.armyOne;
            moveTo("/GUI/addUnits.fxml");
        });

        addUnitsArmyTwo.setOnAction(actionEvent -> {
            AddUnitsController.addUnitsArmy = CreatingTheArmies.armyTwo;
            moveTo("/GUI/addUnits.fxml");
        });

        getInfoAboutFirstArmy.setOnAction(actionEvent -> {
            InfoAboutArmyController.armyInfo = CreatingTheArmies.armyOne;
            moveTo("/GUI/infoAboutArmy.fxml");
        });

        getInfoAboutSecondArmy.setOnAction(actionEvent -> {
            InfoAboutArmyController.armyInfo = CreatingTheArmies.armyTwo;
            moveTo("/GUI/infoAboutArmy.fxml");
        });


        simulateStepByStepButton.setOnAction(actionEvent -> {
            if (!CreatingTheArmies.armyOne.hasUnits() && !CreatingTheArmies.armyTwo.hasUnits()) {
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Armies are empty");
                    customDialog.openDialog();
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hærene er tomme");
                    customDialog.openDialog();
                }
            }
            else if (!CreatingTheArmies.armyOne.hasUnits() && CreatingTheArmies.armyTwo.hasUnits()) {
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Army one is empty");
                    customDialog.openDialog();
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hær en er tom");
                    customDialog.openDialog();
                }
            }
            else if (CreatingTheArmies.armyOne.hasUnits() && !CreatingTheArmies.armyTwo.hasUnits()) {
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Army two is empty");
                    customDialog.openDialog();
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hær to er tom");
                    customDialog.openDialog();
                }
            }
            else if (CreatingTheArmies.armyOne.equals(CreatingTheArmies.armyTwo)){
                CustomDialog customDialog;
                if (CreatingTheArmies.language.equals("ENG")) {
                    customDialog = new CustomDialog("Armies are equal");
                    customDialog.openDialog();
                }else if (CreatingTheArmies.language.equals("NOR")) {
                    customDialog = new CustomDialog("Hærene er like");
                    customDialog.openDialog();
                }
            }
            else {
                terrainType = choiceTerrain.getSelectionModel().getSelectedItem();
                moveTo("/GUI/simulationStepByStep.fxml");
            }
        });

        exitButton.setOnAction(actionEvent -> {
            System.exit(0);
        });

        backToCreatingButton.setOnAction(actionEvent -> {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/GUI/CreatingTheArmies.fxml"));
                Stage window = (Stage) backToCreatingButton.getScene().getWindow();
                window.setScene(new Scene(root));
                CreatingTheArmies.armyOne=null;
                CreatingTheArmies.armyTwo=null;
                versionOfExportFirst = 0;
                versionOfExportSecond = 0;
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        exportFirstArmy.setOnAction(actionEvent -> {
            String path = directoryChooser();
            String exportPath = path+"\\ArmyWithName"+ CreatingTheArmies.armyOne.getName()+
                    "version" + versionOfExportFirst + ".csv";
            if (path!=null){
                CreatingTheArmies.armyOne.writeCsvFile(exportPath);
                versionOfExportFirst++;
            }else {
                String message;
                if (language.equals("ENG")) message = "Directory wasn't chosen";
                else message = "Katalog ble ikke valgt";
                CustomDialog dialog = new CustomDialog(message);
                dialog.openDialog();
            }
        });
        exportSecondArmy.setOnAction(actionEvent -> {
            String path = directoryChooser();
            String exportPath = path+"\\ArmyWithName"+ CreatingTheArmies.armyTwo.getName() +
                    "version" + versionOfExportSecond +".csv";
            if (path!=null){
                CreatingTheArmies.armyTwo.writeCsvFile(exportPath);
                versionOfExportSecond++;
            }else {
                String message;
                if (language.equals("ENG")) message = "Directory wasn't chosen";
                else message = "Katalog ble ikke valgt";
                CustomDialog dialog = new CustomDialog(message);
                dialog.openDialog();
            }
        });
    }

    /**
     * Method which change text according to armies` names
     * and gives it glow animation
     */
    private void textsForNames(){
        nameOfArmyOne.setText(CreatingTheArmies.armyOne.getName());
        nameOfArmyTwo.setText(CreatingTheArmies.armyTwo.getName());
        nameOfArmyOne.setEffect(new Glow(0.9));
        nameOfArmyTwo.setEffect(new Glow(0.9));

    }

    /**
     * Method which set number of units below the icon with type of certain unit
     */
    private void setNumberUnits(){
        if (CreatingTheArmies.armyOne.hasUnits()){
            numberInfantryOne.setText(String.valueOf(CreatingTheArmies.armyOne.getInfantryUnits().size()));
            numberRangedOne.setText(String.valueOf(CreatingTheArmies.armyOne.getRangedUnits().size()));
            numberCavalryOne.setText(String.valueOf(CreatingTheArmies.armyOne.getCavalryUnits().size()));
            numberCommanderOne.setText(String.valueOf(CreatingTheArmies.armyOne.getCommanderUnits().size()));
            numberWizardsOne.setText(String.valueOf(CreatingTheArmies.armyOne.getMagicUnits().size()));

        }else {
            numberInfantryOne.setText("0");
            numberRangedOne.setText("0");
            numberCavalryOne.setText("0");
            numberCommanderOne.setText("0");
            numberWizardsOne.setText("0");
        }
        if (CreatingTheArmies.armyTwo.hasUnits()){
            numberInfantryTwo.setText(String.valueOf(CreatingTheArmies.armyTwo.getInfantryUnits().size()));
            numberRangedTwo.setText(String.valueOf(CreatingTheArmies.armyTwo.getRangedUnits().size()));
            numberCavalryTwo.setText(String.valueOf(CreatingTheArmies.armyTwo.getCavalryUnits().size()));
            numberCommanderTwo.setText(String.valueOf(CreatingTheArmies.armyTwo.getCommanderUnits().size()));
            numberWizardsTwo.setText(String.valueOf(CreatingTheArmies.armyTwo.getMagicUnits().size()));

        }else {
            numberInfantryTwo.setText("0");
            numberRangedTwo.setText("0");
            numberCavalryTwo.setText("0");
            numberCommanderTwo.setText("0");
            numberWizardsTwo.setText("0");
        }
    }

    /**
     * Method which administrate switching on getCertainUnit window
     * with help of method moveTo and initialisation of variable type
     */
    private void getCertainUnit(){
        getInfArmyOne.setOnAction(actionEvent -> {
            GetCertainUnitController.type="INF";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyOne;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getInfArmyTwo.setOnAction(actionEvent -> {
            GetCertainUnitController.type="INF";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyTwo;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getRanArmyOne.setOnAction(actionEvent -> {
            GetCertainUnitController.type="RAN";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyOne;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getRanArmyTwo.setOnAction(actionEvent -> {
            GetCertainUnitController.type="RAN";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyTwo;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getCavArmyOne.setOnAction(actionEvent -> {
            GetCertainUnitController.type="CAV";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyOne;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getCavArmyTwo.setOnAction(actionEvent -> {
            GetCertainUnitController.type="CAV";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyTwo;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getCommArmyOne.setOnAction(actionEvent -> {
            GetCertainUnitController.type="COMM";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyOne;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getCommArmyTwo.setOnAction(actionEvent -> {
            GetCertainUnitController.type="COMM";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyTwo;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getWizardOne.setOnAction(actionEvent -> {
            GetCertainUnitController.type="MAG";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyOne;
            moveTo("/GUI/getCertainUnit.fxml");
        });
        getWizardsTwo.setOnAction(actionEvent -> {
            GetCertainUnitController.type="MAG";
            GetCertainUnitController.armyInfo=CreatingTheArmies.armyTwo;
            moveTo("/GUI/getCertainUnit.fxml");
        });
    }

    /**
     * Method which moves us to another window
     * @param fxmlFile string with fxmlFile`s name
     */
    private void moveTo(String fxmlFile){
        try {
            Parent root = FXMLLoader.load(getClass().getResource(fxmlFile));
            Stage window = new Stage();
            window.getIcons().add(new Image("images/icon.png"));
            window.setTitle("WarGame Application");
            window.initModality(Modality.APPLICATION_MODAL);
            window.setScene(new Scene(root));
            window.showAndWait();
            if (!window.isShowing()){setNumberUnits();}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set english language in Application
     */
    private void setEnglishLanguage(){
        armyOneWithName.setText("Army One with name: ");
        armyTwoWithName.setText("Army Two with name: ");
        hasUnitsOne.setText("has units:");
        hasUnitsTwo.setText("has units:");
        infoLabel.setText("You can get info about certain units only by click on image");
        addUnitsArmyOne.setText("Add units in first army");
        getInfoAboutFirstArmy.setText("Get full info about the army");
        addUnitsArmyTwo.setText("Add units in second army");
        getInfoAboutSecondArmy.setText("Get full info about the army");
        chooseTerrainLabel.setText("Choose terrain");
        simulateButton.setText("Simulate");
        simulateStepByStepButton.setText("Simulate step by step");
        exitButton.setText("Exit");
        backToCreatingButton.setText("Reinitialise armies");
        exportFirstArmy.setText("Export Army");
        exportSecondArmy.setText("Export Army");
        CreatingTheArmies.language="ENG";
    }

    /**
     * Set norwegian language
     */
    private void setNorwegianLanguage(){
        armyOneWithName.setText("Army One med navn: ");
        armyTwoWithName.setText("Army Two med navn: ");
        hasUnitsOne.setText("har enheter:");
        hasUnitsTwo.setText("har enheter:");
        infoLabel.setText("Du kan kun få informasjon om enkelte enheter ved å klikke på bildet");
        addUnitsArmyOne.setText("Legg til enheter i første armé");
        getInfoAboutFirstArmy.setText("Få full informasjon om hæren");
        addUnitsArmyTwo.setText("Legg til enheter i andre hær");
        getInfoAboutSecondArmy.setText("Få full informasjon om hæren");
        chooseTerrainLabel.setText("Velg terreng");
        simulateButton.setText("Simuler");
        simulateStepByStepButton.setText("Simuler trinn for trinn");
        exitButton.setText("Utgang");
        backToCreatingButton.setText("Reinitialiser hærer");

        exportFirstArmy.setText("Eksport hær");
        exportSecondArmy.setText("Eksport hær");

        CreatingTheArmies.language="NOR";

    }

    /**
     * Choose folder for export file
     * @return path to the directory
     */
    public String directoryChooser() {
        DirectoryChooser dc = new DirectoryChooser();

        if (CreatingTheArmies.language.equals("ENG"))
            dc.setTitle("Export army");
        else if (CreatingTheArmies.language.equals("NOR"))
            dc.setTitle("Eksport hær");

        File file = dc.showDialog(null);
        if (file != null) {
            String path = file.getPath();

            return path;
        }
        return null;
    }
}

