package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import wargameclasses.*;

import java.io.File;
import java.time.LocalTime;

import static GUI.CreatingTheArmies.language;

/**
 * The class which administrate result window after the step by step simulation
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class ResultStepByStepController {


    @FXML
    private Button exportData; //button to export winner of simulation

    @FXML
    private Label ArmyWon; //label with text "Army with name won:" on different languages

    @FXML
    private Text unitsAlive; //text "units stayed alive after battle:" on different languages

    @FXML
    private TableColumn<Unit, Integer> attackCol= new TableColumn<>(); //column with attack power of units stayed alive

    @FXML
    private Button backNewDataButton; //button to back to main window with old data

    @FXML
    private Button backOldDataButton = new Button(); //button to back to main window with new data

    @FXML
    private TableColumn<Unit, Integer> defCol= new TableColumn<>(); //column with defense power of units stayed alive

    @FXML
    private TableColumn<Unit, Integer> hpOfUnitCol= new TableColumn<>(); //column with hp units of units stayed alive

    @FXML
    private Text nameOfArmy = new Text(); //name of winner

    @FXML
    private TableColumn<Unit, String> nameOfUnitCol= new TableColumn<>(); //column with names of units stayed alive

    @FXML
    private TableView<Unit> tableView; // table with information about units stayed alive

    /**
     * Method execute then we open this window
     */
    @FXML
    void initialize() {
        nameOfArmy.setText(SimulationStepByStepController.winnerForResult.getName());
        if (CreatingTheArmies.language.equals("ENG"))
            setEnglishLanguage();
        else if (CreatingTheArmies.language.equals("NOR"))
            setNorwegianLanguage();

        buildTable();




        backNewDataButton.setOnAction(actionEvent -> {
            CreatingTheArmies.armyOne= SimulationStepByStepController.armyOneForResult;
            CreatingTheArmies.armyTwo= SimulationStepByStepController.armyTwoForResult;
            SimulationStepByStepController.armyOneForResult = null;
            SimulationStepByStepController.armyTwoForResult = null;
            SimulationStepByStepController.winnerForResult = null;

            Stage stage = (Stage) backNewDataButton.getScene().getWindow();
            stage.close();

        });
        backOldDataButton.setOnAction(actionEvent -> {
            Stage stage = (Stage) backOldDataButton.getScene().getWindow();
            stage.close();
        });

        exportData.setOnAction(actionEvent -> {
            String path = directoryChooser();
            if (path!=null){
                String exportPath = path+"\\simulationResultOf"+ LocalTime.now().getHour()+
                        "-" + LocalTime.now().getMinute()+"-"+ LocalTime.now().getSecond() + ".csv";
                SimulationStepByStepController.winnerForResult.writeCsvFile(exportPath);
            }else {
                String message;
                if (language.equals("ENG")) message = "Directory wasn't chosen";
                else message = "Katalog ble ikke valgt";
                CustomDialog dialog = new CustomDialog(message);
                dialog.openDialog();
            }
        }
        );



    }

    /**
     * Method which build tableView with information about the winner
     */
    void buildTable(){
        nameOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,String>("name"));
        hpOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("health"));
        attackCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("attack"));
        defCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("armor"));
        ObservableList<Unit> unitList = FXCollections.observableArrayList();
        unitList.addAll(SimulationStepByStepController.winnerForResult.getAllUnits());
        tableView.setItems(unitList);
    }


    /**
     * Set english language in window
     */

    public void setEnglishLanguage(){
        ArmyWon.setText("Army with name won: ");
        unitsAlive.setText("units stayed alive after battle:");
        backNewDataButton.setText("Back with new data");
        backOldDataButton.setText("Back with old data");
        nameOfUnitCol.setText("Name");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Attack");
        defCol.setText("Defense");
        exportData.setText("Export result army");
    }
    /**
     * Set norwegian language in window
     */
    public void setNorwegianLanguage(){
        ArmyWon.setText("Hæren med navn vant: ");
        unitsAlive.setText("enheter holdt seg i live etter kamp:");
        backNewDataButton.setText("Tilbake med nye data");
        backOldDataButton.setText("Tilbake med gamle data");
        nameOfUnitCol.setText("Navn");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Angrep");
        defCol.setText("Forsvar");
        exportData.setText("Eksport resultat hær");
    }

    /**
     * Choose folder for export file
     * @return path to the directory
     */
    public String directoryChooser() {
        DirectoryChooser dc = new DirectoryChooser();

        if (CreatingTheArmies.language.equals("ENG"))
            dc.setTitle("Export winner army");
        else if (CreatingTheArmies.language.equals("NOR"))
            dc.setTitle("Eksport vinner hær");

        File file = dc.showDialog(null);
        if (file != null) {
            String path = file.getPath();

            return path;
        }
        return null;
    }
}
