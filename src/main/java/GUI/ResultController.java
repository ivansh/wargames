package GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;

import javafx.stage.Stage;
import wargameclasses.*;

import java.io.File;
import java.time.LocalTime;
import java.util.ArrayList;

import static GUI.CreatingTheArmies.language;

/**
 * The class which administrate result window after the simulation
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class ResultController {
    private Army armyOneCopy = null; //copy of first army
    private Army armyTwoCopy = null; //copy of second army

    public Army winner = null; //winner of simulation

    @FXML
    private Button exportData; //button to export winner of simulation

    @FXML
    private Label ArmyWon; //label with text "Army with name won:" on different languages

    @FXML
    private Text unitsAlive; //text "units stayed alive after battle:" on different languages

    @FXML
    private TableColumn<Unit, Integer> attackCol= new TableColumn<>(); //column with attack power of units stayed alive

    @FXML
    private Button backNewDataButton; //button to back to main window with old data

    @FXML
    private Button backOldDataButton = new Button(); //button to back to main window with new data

    @FXML
    private TableColumn<Unit, Integer> defCol= new TableColumn<>(); //column with defense power of units stayed alive

    @FXML
    private TableColumn<Unit, Integer> hpOfUnitCol= new TableColumn<>(); //column with hp units of units stayed alive

    @FXML
    private Text nameOfArmy = new Text(); //name of winner

    @FXML
    private TableColumn<Unit, String> nameOfUnitCol= new TableColumn<>(); //column with names of units stayed alive

    @FXML
    private TableView<Unit> tableView; // table with information about units stayed alive

    /**
     * Method execute then we open this window
     */
    @FXML
    void initialize() {
        if (language.equals("ENG"))
            setEnglishLanguage();
        else if (language.equals("NOR"))
            setNorwegianLanguage();
        creatingOfCopies();


        Battle battle = new Battle(CreatingTheArmies.armyOne,CreatingTheArmies.armyTwo);
        switch (MainWindowController.terrainType){
            case "NOTERRAIN":
               battle.setTerrainInBattle(TerrainType.NOTERRAIN);
               break;
            case "HILL":
                battle.setTerrainInBattle(TerrainType.HILL);
                break;
            case "PLAINS":
                battle.setTerrainInBattle(TerrainType.PLAINS);
                break;
            case "FOREST":
                battle.setTerrainInBattle(TerrainType.FOREST);
                break;

        }

        try {
            winner=battle.simulate();

        } catch (Exception e) {
            CustomDialog customDialog = new CustomDialog(e.getMessage());
            customDialog.openDialog();
        }
        buildTable();
        if (winner!=null)
            nameOfArmy.setText(winner.getName());


        backNewDataButton.setOnAction(actionEvent -> {
            Stage stage = (Stage) backOldDataButton.getScene().getWindow();
            stage.close();
        });
        backOldDataButton.setOnAction(actionEvent -> {
            CreatingTheArmies.armyOne = armyOneCopy;

            CreatingTheArmies.armyTwo = armyTwoCopy;
            Stage stage = (Stage) backOldDataButton.getScene().getWindow();
            stage.close();
        });

        exportData.setOnAction(actionEvent -> {
            String path = directoryChooser();
            if (path!=null){
                String exportPath = path +"\\simulationResultOf"+ LocalTime.now().getHour()+
                        "-" + LocalTime.now().getMinute()+"-"+ LocalTime.now().getSecond() + ".csv";
                winner.writeCsvFile(exportPath);
            }else {
                String message;
                if (language.equals("ENG")) message = "Directory wasn't chosen";
                else message = "Katalog ble ikke valgt";
                CustomDialog dialog = new CustomDialog(message);
                dialog.openDialog();
            }
        }
        );



    }

    /**
     * Method which build tableView with information about the winner
     */
    void buildTable(){
        nameOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,String>("name"));
        hpOfUnitCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("health"));
        attackCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("attack"));
        defCol.setCellValueFactory(new PropertyValueFactory<Unit,Integer>("armor"));
        ObservableList<Unit> unitList = FXCollections.observableArrayList();
        unitList.addAll(winner.getAllUnits());
        tableView.setItems(unitList);
    }

    /**
     * Method which creates copies of armies
     * before simulation
     * We need it to have possibility to return with old data
     */
    void creatingOfCopies(){
        ArrayList<Unit> listOfArmyOne = new ArrayList<>();
        for (Unit unit: CreatingTheArmies.armyOne.getInfantryUnits()){
            unit = new InfantryUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getRangedUnits()){
            unit = new RangedUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getCavalryUnits()){
            unit = new CavalryUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getCommanderUnits()){
            unit = new CommanderUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyOne.getMagicUnits()){
            unit = new MagicUnit(unit.getName(), unit.getHealth());
            listOfArmyOne.add(unit);
        }

        ArrayList<Unit> listOfArmyTwo = new ArrayList<>();
        for (Unit unit: CreatingTheArmies.armyTwo.getInfantryUnits()){
            unit = new InfantryUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getRangedUnits()){
            unit = new RangedUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getCavalryUnits()){
            unit = new CavalryUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getCommanderUnits()){
            unit = new CommanderUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }
        for (Unit unit: CreatingTheArmies.armyTwo.getMagicUnits()){
            unit = new MagicUnit(unit.getName(), unit.getHealth());
            listOfArmyTwo.add(unit);
        }


        armyOneCopy = new Army(CreatingTheArmies.armyOne.getName(),listOfArmyOne);
        armyTwoCopy = new Army(CreatingTheArmies.armyTwo.getName(),listOfArmyTwo);
    }

    /**
     * Set english language in window
     */

    public void setEnglishLanguage(){
        ArmyWon.setText("Army with name won: ");
        unitsAlive.setText("units stayed alive after battle:");
        backNewDataButton.setText("Back with new data");
        backOldDataButton.setText("Back with old data");
        nameOfUnitCol.setText("Name");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Attack");
        defCol.setText("Defense");
        exportData.setText("Export result army");
    }
    /**
     * Set norwegian language in window
     */
    public void setNorwegianLanguage(){
        ArmyWon.setText("Hæren med navn vant: ");
        unitsAlive.setText("enheter holdt seg i live etter kamp:");
        backNewDataButton.setText("Tilbake med nye data");
        backOldDataButton.setText("Tilbake med gamle data");
        nameOfUnitCol.setText("Navn");
        hpOfUnitCol.setText("HP");
        attackCol.setText("Angrep");
        defCol.setText("Forsvar");
        exportData.setText("Eksport resultat hær");
    }

    /**
     * Choose folder for export file
     * @return path to the directory
     */
    public String directoryChooser() {
        DirectoryChooser dc = new DirectoryChooser();

        if (language.equals("ENG"))
            dc.setTitle("Export winner army");
        else if (language.equals("NOR"))
            dc.setTitle("Eksport vinner hær");

        File file = dc.showDialog(null);
        if (file != null) {
            String path = file.getPath();

            return path;
        }
        return null;
    }
}
