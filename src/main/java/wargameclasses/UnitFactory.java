package wargameclasses;

import java.util.ArrayList;

/**
 * The design pattern which
 * Can create all instance of units(infantry,ranged,cavalry and commander)
 *
 * This pattern delegates the responsibility of initializing a class
 * from the client to a particular factory class by creating a type of virtual constructor.
 * (https://www.baeldung.com/creational-design-patterns#:~:text=The%20Factory%20Design%20Pattern%20or,class%20defer%20instantiation%20to%20subclasses%E2%80%9D.)
 *
 * it takes constant types from UnitType
 * @see UnitType
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public class UnitFactory {

    /**
     * Static method which create Unit according it type, name and amount of health point
     * @param type type of unit in UnitType
     * @param name name of unit
     * @param hp health of unit
     * @return new Unit
     */
    public static Unit getUnit(UnitType type,String name, int hp){
        switch (type){
            case INFANTRY:
                return new InfantryUnit(name,hp);
            case RANGED:
                return new RangedUnit(name,hp);
            case CAVALRY:
                return new CavalryUnit(name,hp);
            case COMMANDER:
                return new CommanderUnit(name,hp);
            case MAGIC:
                return new MagicUnit(name,hp);
        }return null;
    }
    /**
     * Static method which create list with certain number of
     * Units according it type, name and amount of health point
     * @param type type of unit in UnitType
     * @param name name of unit
     * @param hp health of unit
     * @param number how many units in list
     * @return new Unit
     * @throws IllegalArgumentException if number of Units is negative or 0
     */
    public static ArrayList<Unit> getUnitsList(UnitType type, String name, int hp,int number){
        if (number<=0) throw new IllegalArgumentException("Enter the valid number of units in list(more than 0)");
        ArrayList<Unit> units = new ArrayList<>();
        for (int i = 0; i<number;i++){
            units.add(getUnit(type,name,hp));
        }return units;
    }
}
