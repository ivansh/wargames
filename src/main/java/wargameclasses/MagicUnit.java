package wargameclasses;

import java.util.Random;

/**
 * The subclass of our game which describe Magic Unit-object and it`s methods
 * @author Kandidatnr:10049
 * @version 1.0.1
 * @see Unit
 */
public class MagicUnit extends Unit{

    private int counter=0; //variable which helps us to realise one of MagicUnit`s special mechanics

    /**
     * Usual constructor for MagicUnit- instance
     * if we will describe each variable self
     * @param name name of MagicUnit
     * @param health hp of MagicUnit
     * @param attack MagicUnit`s damage
     * @param armor MagicUnit`s armor
     * @throws IllegalArgumentException if hp < 0
     */
    public MagicUnit(String name, int health, int attack, int armor){
        super(name, health, attack, armor);

    }
    /**
     * Simplified constructor for MagicUnit- instance with
     * certain attack variable(15) and armor variable(8)
     * @param name name of MagicUnit
     * @param health hp of MagicUnit
     * @throws IllegalArgumentException if hp < 0
     */
    public MagicUnit(String name, int health){
        super(name, health,15,8);
    }

    /**
     * Magic Unit has a special mantle that reflects the first blow and gives 10 defense bonus,
     * but then it disappears magic units doesn't get any defense bonus
     * In our game MagicUnit has special mechanics
     * His resist bonus depends on how many times he has been attacked by opponent
     * For counting how many times he attacked we override setHealth() method, because w
     * we apply it to the object which was attacked by opponent
     * counter variable add 1 every time MagicUnit being attacked,
     * so we can count how many times he has been attacked with help of counter-variable
     * @param health health of Magic Unit
     */
    @Override
    public void setHealth(int health) {
        super.setHealth(health);
        counter++;
    }

    /**
     * Override of getAttackBonus() method which return additional attack because of
     * advantages of different types of army
     * Magic unit has 2 different spells: fireball and iceball
     * Unit use spell accidentally
     *
     * @return random attack depends on terrain : fireball gives more damage in forest and less in Plains
     * and iceball gives more damage on hills, but less in plains
     */
    @Override
    public int getAttackBonus() {
        Random rand = new Random();
        int random = rand.nextInt(2);

        if (random==0){ //fireball
            if (getTerrainType().equals(TerrainType.FOREST)){
                return 10;
            }else if (getTerrainType().equals(TerrainType.PLAINS)){
                return 5;
            }else return 7;
        }else {
            if (getTerrainType().equals(TerrainType.HILL)){
                return 9;
            }else if (getTerrainType().equals(TerrainType.PLAINS)){
                return 4;
            }else return 6;
        }
    }

    /**
     * Special Resist Bonus mechanics for Magic Unit
     * based on how many times he has attacked
     * @return 10 if Magic Unit were attacked before and 0 if it attacks after
     */
    @Override
    public int getResistBonus() {
        if (this.counter==0) return 10;
        else return 0;
    }
}
