package wargameclasses;

/**
 * The subclass of our game which describe RangedUnit-object and it`s methods
 * @author Kandidatnr:10049
 * @version 1.0.1
 * @see Unit
 */
public class RangedUnit extends Unit{

    private int counter=0; //variable which helps us to realise RangedUnit special mechanics

    /**
     * Usual constructor for RangedUnit- instance
     * if we will describe each variable self
     * @param name name of RangedUnit
     * @param health hp of RangedUnit
     * @param attack RangedUnit`s damage
     * @param armor RangedUnit`s armor
     * @throws IllegalArgumentException if hp < 0
     */
    public RangedUnit (String name, int health,int attack, int armor){
        super(name, health, attack, armor);

    }
    /**
     * Simplified constructor for RangedUnit- instance with
     * certain attack variable(15) and armor variable(8)
     * @param name name of RangedUnit
     * @param health hp of RangedUnit
     * @throws IllegalArgumentException if hp < 0
     */
    public RangedUnit (String name, int health){
        super(name, health,15,8);
    }

    /**
     * In our game RangedUnit has special mechanics
     * His resist bonus depends on how many times he has been attacked by opponent
     * For counting how many times he attacked we override setHealth() method, because w
     * we apply it to the object which was attacked by opponent
     * counter variable add 1 every time RangedUnit being attacked,
     * so we can count how many times he has been attacked with help of counter-variable
     * @param health health of RangedUnit
     */
    @Override
    public void setHealth(int health) {
        super.setHealth(health);
        counter++;
    }

    /**
     * Override of getAttackBonus() method which return additional attack because of
     * advantages of different types of army
     * @return 3 for RangedUnit
     */
    @Override
    public int getAttackBonus() {
        if (getTerrainType().equals(TerrainType.HILL)) return 3+3; //3 extra attack because terrain is hills
        else if (getTerrainType().equals(TerrainType.FOREST)) return 3-1; //decrease attack bonus because terrain is forest
        else return 3;
    }

    /**
     * Special Resist Bonus mechanics for RangedUnit
     * based on how many times he has attacked
     * @return 6 if Ranged Unit been attacked for the first time
     *         4 if Ranged Unit been attacked for the second time
     *         2 if Ranged Unit been attacked for then two times
     */
    @Override
    public int getResistBonus() {
        if (this.counter==0) return 6;
        else if (this.counter==1) return 4;
        else return 2;
    }
}
