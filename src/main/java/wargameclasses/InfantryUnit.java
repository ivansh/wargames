package wargameclasses;

/**
 * The subclass of our game which describe InfantryUnit - object and it`s methods
 * @author Kandidatnr:10049
 * @version 1.0.0
 * @see Unit
 */
public class InfantryUnit extends Unit {

    /**
     * Usual constructor for InfantryUnit- object
     * if we will describe each variable self
     * @param name name of InfantryUnit
     * @param health hp of InfantryUnit
     * @param attack InfantryUnit`s damage
     * @param armor InfantryUnit`s armor
     * @throws IllegalArgumentException if hp<0
     */
    public InfantryUnit (String name, int health,int attack, int armor){
        super(name, health, attack, armor);
    }

    /**
     * Simplified constructor for InfantryUnit- object with
     * certain attack variable(15) and armor variable(10)
     * @param name name of InfantryUnit
     * @param health hp of InfantryUnit
     * @throws IllegalArgumentException if hp < 0
     */
    public InfantryUnit (String name, int health){
        super(name, health,15,10);
    }

    /**
     * Override of getAttackBonus() method which return additional attack because of
     * advantages of different types of armor
     * @return 2 for InfantryUnit
     */
    @Override
    public int getAttackBonus() {
        if (getTerrainType().equals(TerrainType.FOREST)) return 2+2; //+2 bonus if battle in forest
        else return 2;
    }

    /**
     * Override of getResistBonus() method which return additional armor because of
     * advantages of different types of army
     * @return 1 for InfantryUnit
     */
    @Override
    public int getResistBonus() {
        if (getTerrainType().equals(TerrainType.FOREST)) return 1+2; //+2 bonus if battle in forest
        else return 1;
    }



}
