package wargameclasses;

/**
 *Types of terrain in which battle can take place
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public enum TerrainType {
    HILL,
    PLAINS,
    FOREST,
    NOTERRAIN
}
