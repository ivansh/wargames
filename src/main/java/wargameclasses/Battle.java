package wargameclasses;

/**
 * Class which administrate battle between 2 armies
 * Central in this class is stimulating() method
 * @author Kandidatnr:10049
 * @version 1.0.0
 * @see Army
 */
public class Battle {

    private Army armyOne; //variable which describe first army object which will participate in battle
    private Army armyTwo; //variable which describe second army object which will participate in battle

    /**
     * Standard Constructor for Battle between to army
     * @param armyOne first army
     * @param armyTwo second army
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * Additional constructor that takes into account the
     * type of terrain on which the battle takes place
     * @param armyOne first army
     * @param armyTwo second army
     * @param terrain terrain in which battle take place in
     */
    public Battle (Army armyOne,Army armyTwo, TerrainType terrain) {
        armyOne.getAllUnits().forEach(p->p.setTerrainType(terrain));
        armyTwo.getAllUnits().forEach(p->p.setTerrainType(terrain));

        this.armyOne = armyOne;
        this.armyTwo = armyTwo;

    }

    /**
     * Set terrain to battle or change it
     * @param terrain terrain of the battle
     */
    public void setTerrainInBattle(TerrainType terrain){
        armyOne.getAllUnits().forEach(p->p.setTerrainType(terrain));
        armyTwo.getAllUnits().forEach(p->p.setTerrainType(terrain));
    }

    /**
     * Method which gives us full information about armies who participated in battle
     * @return string with info about the army
     */
    public String toString() {
        return "Battle between to army: \n " +
                "FIRST ARMY:\n" + armyOne.toString() +
                "\nSECOND ARMY:\n" + armyTwo.toString();
    }

    /**
     * The main method of Battle-object
     * This is method which will stimulate battle between to armies
     * The simulation is relatively simple: a random unit from an army attacking
     * random unit from the second army. If a unit has health equal to 0 it is removed from army.
     * This pattern is repeated until one of the army is eliminated. In the end, the winner is returned
     * (armyOne or armyTwo)
     * @return the winner of the battle
     * @throws  throw exception "Draw" if it is draw in battle
     *          throw exception "Armies are equal" if we have 2 equal armies
     *          throw exception "Armies are empty" if we don`t have units in armies
     */
    public Army simulate() throws Exception{

        Unit defenderArmyOne;   //we initialise two instance which will be attacked from second army "Defender of the first army"
        Unit defenderArmyTwo;   // and the first army  "Defender of the second army"

        if (!armyOne.hasUnits() && armyTwo.hasUnits()) return armyTwo;
        else if (!armyTwo.hasUnits() && armyOne.hasUnits()) return armyOne;
        else if (!armyOne.hasUnits() && !armyTwo.hasUnits()) throw new Exception("Armies are empty");

        if (armyOne.equals(armyTwo)) throw new Exception("Armies are equal");

        while (armyOne.hasUnits() || armyTwo.hasUnits()) {  //if one army not equals second we make loop
            defenderArmyOne = armyTwo.getRandom(); //we take defenders from armies
            defenderArmyTwo = armyOne.getRandom();

            armyOne.getRandom().attack(defenderArmyOne); //random unit from first army attacks random unit from the second
            if (defenderArmyOne.getHealth() <= 0)
                armyTwo.remove(defenderArmyOne); //if defender is dead (0 hp) we delete it from our collection

            if (!armyTwo.hasUnits() && armyOne.hasUnits()) return armyOne;


            armyTwo.getRandom().attack(defenderArmyTwo);    //the same but with second army
            if (defenderArmyTwo.getHealth() <= 0) armyOne.remove(defenderArmyTwo);

            if (!armyOne.hasUnits() && armyTwo.hasUnits()) return armyTwo;
        }
        throw new Exception("Draw");
    }
    }

