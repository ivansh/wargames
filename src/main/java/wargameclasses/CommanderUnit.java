package wargameclasses;

/**
 * The subclass of our game which describe CavalryUnit-object and it`s methods
 * @author Kandidatnr:10049
 * @version 1.0.0
 * @see Unit
 */
public class CommanderUnit extends CavalryUnit{
    /**
     * Usual constructor for CommanderUnit  - instance
     * if we will describe each variable self
     * @param name name of CommanderUnit
     * @param health hp of CommanderUnit
     * @param attack CommanderUnit`s damage
     * @param armor CommanderUnit`s armor
     * @throws IllegalArgumentException if hp < 0
     */
    public CommanderUnit (String name, int health,int attack, int armor){
        super(name, health, attack, armor);
    }

    /**
     * Simplified constructor for CavalryUnit - instance with
     * certain attack variable(25) and armor variable(15)
     * @param name name of CommanderUnit
     * @param health hp of CommanderUnit
     * @throws IllegalArgumentException if hp < 0
     */
    public CommanderUnit (String name, int health){
        super(name, health,25,15);
    }
}
