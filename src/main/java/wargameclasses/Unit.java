package wargameclasses;

import java.util.Objects;

/**
 * The superclass of our program class
 * one superclass for all unique types of armies which we will extend army from it,
 * We make it abstract, because it cannot be instantiated
 *
 * In the start Unit has not special terrain - so it has terrain type equals NOTERRAIN
 * In the battle we need to initialise it (with command setTerrainType) or stay without changes
 * @author Kandidatnr:10049
 * @version 1.0.1
 */
public abstract class Unit {
    private final String name;    //variable which define name variable of Unit
    private int health;           //variable which define number of health-point which Unit has
    private final int attack;     //variable which define damage-point which Unit has
    private final int armor;      //variable which define armor-point which Unit has

    private TerrainType terrainType = TerrainType.NOTERRAIN; //variable which describe type of terrain

    /**
     * Creating Unit - instance
     * Number of health points can`t be negative, so method throws IllegalArgumentException
     * Method is protected because we won`t override it in subclasses
     * It has protected-modification because only subclasses will use Unit`s-constructor
     * @param name name of unit
     * @param health hp of unit
     * @param attack attack of unit
     * @param armor armor of unit
     * @throws IllegalArgumentException if health is given by negative number
     */
    protected Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        this.name = name;
        if (health<=0) throw new IllegalArgumentException("Health can`t be negative");
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }


    /**
     *The method which describe attack of unit on the opponent
     * if opponent will be attacked and lost hp , så it realised with help of special formula
     * if opponent will have more defense-power then attacker has attack,it will not change hp
     * else it will not change hp of defender
     * if we won't check it, after attack opponent can get more hp than it has
     * @param opponent opponent who will be attacked by our unit
     */
    public void attack(Unit opponent){
        if ((this.attack+this.getAttackBonus())>= ((opponent.getArmor()+opponent.getResistBonus())))
            opponent.setHealth(opponent.getHealth()-(this.attack+this.getAttackBonus())+(opponent.getArmor()+opponent.getResistBonus()));
    }

    /**
     * Accessor method for name variable
     * @return String name-variable
     */
    public String getName() {
        return name;
    }

    /**
     * Accessor method for health variable
     * @return int health-variable
     */
    public int getHealth() {
        return health;
    }

    /**
     * Accessor method for attack variable
     * @return int attack-variable
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Accessor method for armor variable
     * @return int armor-variable
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Mutator method for health variable
     * @param health hp of our Unit
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Method which gives us possibility to change
     * terrain
     * @param terrainType type of terrain
     */
    public void setTerrainType(TerrainType terrainType){
        this.terrainType= terrainType;
    }
    /**
     * Method which gives us access to terrain
     * @return terrain
     */
    public TerrainType getTerrainType(){
        return terrainType;
    }


    /**
     * Abstract method which will give attack bonus because of unit-army`s specialisation
     * @return int attackBonus
     */
    public abstract int getAttackBonus();

    /**
     * Abstract method which will give resist bonus (increase defence) because of unit-army`s specialisation
     * @return int attackBonus
     */
    public abstract int getResistBonus();

    /**
     * toString method for Unit class,
     * which will return information about Unit in String type
     * @return string with full information about unit
     */
    @Override
    public String toString() {
        return  "Name of Unit: " + name + "\n" +
                "Hp: " + health + "\n" +
                "Attack power: " + attack + "\n" +
                "Armor: " + armor + "\n" ;
    }
    public TerrainType getTerrain(){
        return terrainType;
    }

    /**
     * Usual method for comparing two unit-objects
     * @param o object comparing
     * @return true or false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Unit)) return false;
        Unit unit = (Unit) o;
        return health == unit.health && attack == unit.attack && armor == unit.armor && Objects.equals(name, unit.name);
    }



}
