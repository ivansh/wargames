package wargameclasses;

/**
 * All types of Units
 * Constant values
 * @author Kandidatnr:10049
 * @version 1.0.0
 */
public enum UnitType {
    INFANTRY,
    RANGED,
    CAVALRY,
    COMMANDER,
    MAGIC

}
