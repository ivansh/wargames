package wargameclasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Class which administrate collection of units that can attack other units in a battle
 * We use java.util.List for collecting units
 * I use ArrayList here for collection of units
 * @author Kandidatnr:10049
 * @version 1.0.0
 * @see Unit
 */
public class Army {

    private final String name; //name of Army
    private List<Unit> units = new ArrayList<>(); //collection of units

    /**
     * Constructor for creating Army-object
     * @param name name of army
     * @param units collection of units
     */
    public Army(String name, List<Unit> units) {
        this.name = name;
        this.units = units;
    }

    /**
     * Constructor for creating Army-object based on only name
     */
    public Army(String name) {
        this.name = name;
    }

    /**
     * Method (accessor) which help us get name of the Army
     */
    public String getName() {
        return name;
    }

    /**
     * Method which will add units in our army
     * Units can repeat
     * @param unit unit which we want to add in our army
     */
    public void add(Unit unit){
        this.units.add(unit);
    }

    /**
     * Method which takes ready collection and add it in our army
     * @param units Collection of units
     */
    public void addAll(List<Unit> units){
        for (Unit unit:units){
            this.units.add(unit);
        }
    }

    /**
     * Method which delete one unit from collection
     * When we find this unit we delete it from collection
     * @param unit which we want to remove
     */
    public void remove(Unit unit){
        units.remove(unit);
    }

    /**
     * Method which checks if we have units in our Army
     * @return true if army has units, false if it has not
     */
    public boolean hasUnits(){
        return !units.isEmpty();
    }

    /**
     * Method (accessor) which return full collection of our units
     * @return collection of units
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Method which return random unit from our army
     * We use java.util.Random for generate random number between 0 and size of array-1
     * Because then we take unit from list its number starts with 0
     * and ends with size-1 we don`t need to make extra operations in getting random number
     * @return random unit
     */
    public Unit getRandom(){
        Random rand = new Random();
        int n = rand.nextInt(0,units.size());
        return units.get(n);
    }

    /**
     * Method which return string with full information about army
     * @return String with full inf about army
     */
    @Override
    public String toString() {
        String str= "";
        int i = 1;
        if (units.size()!=0){
            for (Unit unit:units){
                str+= i +". "+ unit.toString();
                i++;
        }return "Army with name " + name.toUpperCase() + " has units :\n" + str;}
        else return "Army with name " + name.toUpperCase() + " has no units";
    }

    /**
     * Method which compares to armies
     * @param army army which we need to compare with another
     * @return true if armies equals
     */
    public boolean equals(Army army) {
        boolean result = false;
        int i=0;
        if (!this.name.equals(army.getName())) return false; //if armies have different names method returns false
        if (army.getAllUnits().size()!=units.size()) return false;  //if armies has different size of units collections armies are not equal
        army.getAllUnits().sort(Comparator.comparing(Unit::getName));   //we sort armies
        units.sort(Comparator.comparing(Unit::getName));
        for (Unit unit:units){
            result=unit.equals(army.getAllUnits().get(i));
            i++;
        }return result;
    }

    /**
     * Method which gives hashCOde of Army-Object
     * @return hashCode of our Army-project
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }

    /**
     * Method which return all Infantry Units
     * in one collection
     * @return all Infantry Units in one collection
     */
    public List<Unit> getInfantryUnits() {
        return units.stream().filter(unit ->unit instanceof InfantryUnit).toList();
    }

    /**
     * Method which return all Magic Units
     * in one collection
     * @return all Magic Units in one collection
     */
    public List<Unit> getMagicUnits() {
        return units.stream().filter(unit ->unit instanceof MagicUnit).toList();
    }

    /**
     * Method which return all Ranged Units
     * in one collection
     * @return all Ranged Units in one collection
     */
    public List<Unit> getRangedUnits() {
        return units.stream().filter(unit ->unit instanceof RangedUnit).toList();
    }
    /**
     * Method which return all Commander Units
     * in one collection
     * @return all Commander Units in one collection
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(unit ->unit instanceof CommanderUnit).toList();
    }
    /**
     * If we use the same sorting as in previous methods
     * we will get collection with Commander Units and CavalryUnits
     * because Commander Unit is subclass of Cavalry Units
     * and all Commander units instance of Cavalry Unit == true
     * .toList() creates Immutable list which you can`t change
     * so we create a new mutable list and addAll components in it
     * @return cavalry units
     */
    public List<Unit> getCavalryUnits() {

        List<Unit> cavalryUnitsAndCommanderUnits = new ArrayList<>();
        cavalryUnitsAndCommanderUnits.addAll(units.stream().filter(unit ->unit instanceof CavalryUnit).toList());

        List<Unit> commanderUnits = units.stream().filter(unit ->unit instanceof CommanderUnit).toList();
        cavalryUnitsAndCommanderUnits.removeAll(commanderUnits);
        return cavalryUnitsAndCommanderUnits;
    }

    /**
     * Method that makes it possible to read an army from a file
     * it is static,because we use it for constructor
     * @param path path to file
     * @return Army generated from csv file
     * @throws FileNotFoundException if file not found
     */
    public static Army readCsvFile(String path) throws Exception {
        File file = new File(path);

        try (Scanner scanner = new Scanner(file)){

            String nameOfArmy = scanner.nextLine();
            Army army= new Army(nameOfArmy);

            while (scanner.hasNext()){
                String line = scanner.nextLine();
                String[] list = line.split(",");
                if (list.length!=3) throw new Exception("Wrong format");
                String type = list[0];
                switch (type){
                    case "InfantryUnit":
                        army.add(new InfantryUnit(list[1],Integer.valueOf(list[2])));
                        break;
                    case "RangedUnit":
                        army.add(new RangedUnit(list[1],Integer.valueOf(list[2])));
                        break;
                    case "CavalryUnit":
                        army.add(new CavalryUnit(list[1],Integer.valueOf(list[2])));
                        break;
                    case "CommanderUnit":
                        army.add(new CommanderUnit(list[1],Integer.valueOf(list[2])));
                        break;
                    case "MagicUnit":
                        army.add(new MagicUnit(list[1],Integer.valueOf(list[2])));
                        break;
                }
            }return army;

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        return null;
    }

    /**
     * Method which take name of file in which we want to write units of army
     * @param path path to file
     */
    public void writeCsvFile(String path){
        File file = new File(path);
        try (FileWriter fileWriter = new FileWriter(file)) {
            String infoAboutArmy = null;
            infoAboutArmy= name +"\n";
            for (Unit unit:units){
                if (unit instanceof InfantryUnit) infoAboutArmy+="InfantryUnit,";
                else if (unit instanceof RangedUnit) infoAboutArmy+="RangedUnit,";
                else if (unit instanceof CommanderUnit) infoAboutArmy+="CommanderUnit,";
                else if (unit instanceof MagicUnit) infoAboutArmy+="MagicUnit,";
                else infoAboutArmy+="CavalryUnit,";
                infoAboutArmy+=unit.getName() +"," +unit.getHealth()+"\n";
            }fileWriter.write(infoAboutArmy);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    }
