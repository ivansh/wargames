package wargameclasses;

/**
 * The subclass of our game which describe CavalryUnit-object and it`s methods
 * @author Kandidatnr:10049
 * @version 1.0.0
 * @see Unit
 */
public class CavalryUnit extends Unit{


    private int counter = 0;//special variable which helps us to count how many times CavalryUnit attacked enemy

    /**
     * Usual constructor for CavalryUnit - object
     * if we will describe each variable self
     * @param name name of CavalryUnit
     * @param health hp of CavalryUnit
     * @param attack CavalryUnit`s damage
     * @param armor CavalryUnit`s armor
     * @throws IllegalArgumentException if hp < 0
     */
    public CavalryUnit (String name, int health,int attack, int armor){
        super(name, health, attack, armor);
    }
    /**
     * Simplified constructor for CavalryUnit - object with
     * certain attack variable(20) and armor variable(12)
     * @param name name of CavalryUnit
     * @param health hp of CavalryUnit
     * @throws IllegalArgumentException if hp < 0
     */
    public CavalryUnit (String name, int health){
        super(name, health,20,12);
    }
    /**
     * In our game CavalryUnit has special mechanics
     * His attack bonus depends on how many times he attacked opponent
     * For counting how many times he attacked we override attack() method
     * we take main construction and formula from super class,but also use
     * counter variable and add to it 1 every time CavalryUnit attacks,
     * so we can count how many times he attacked with help of counter-variable
     * @param opponent opponent which will attack unit
     */
    @Override
    public void attack(Unit opponent) {
        super.attack(opponent);
        this.counter++;
    }
    /**
     * Override of getAttackBonus() method which return additional attack because of
     * advantages of different types of army
     * When CavalryUnit attacks first time it gets attack bonus 6
     * then second etc. only 2
     * @return 6 or 2 depends on situation
     */
    @Override
    public int getAttackBonus() {
        if (getTerrainType().equals(TerrainType.PLAINS)){ //+2 attack if we fight in plains biom
            if (counter==0) return (4+4);
            else return 4;

        }else {
            if (counter==0) return (4+2);
            else return 2;
        }
    }

    /**
     * Override of getResistBonus() method which return additional armor because of
     * advantages of different types of army
     * @return 1 for CavalryUnit
     */
    @Override
    public int getResistBonus() {
        if (getTerrainType().equals(TerrainType.FOREST)) return 0; //it doesn't has defense bonus in forest
        else return 1;
    }
}
