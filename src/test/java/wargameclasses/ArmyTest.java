package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {
    @Nested
    class hasUnitsTest{
        /**
         * In that test we check not only hasUnits() method
         * but also standard constructor
         */
        @Test
        public void armyHasUnits(){
            List<Unit> units = new ArrayList<>();
            units.add(new CavalryUnit("Test Cavalry",1));
            Army army= new Army("Test",units);
            assertTrue(army.hasUnits());
        }
        @Test
        public void armyHasNoUnits(){
            Army army= new Army("Test");
            assertFalse(army.hasUnits());
        }
    }
    @Test
    public void testAddAll(){
        List<Unit> units = new ArrayList<>();
        units.add(new CavalryUnit("Test Cavalry",1));
        Army army= new Army("Test");
        army.addAll(units);
        assertEquals(army.getAllUnits(),units);
    }
    @Test
    public void testGetAllUnits(){
        List<Unit> units = new ArrayList<>();
        units.add(new CavalryUnit("Test Cavalry",1));
        Army army= new Army("Test");
        army.addAll(units);
        assertEquals(army.getAllUnits(),units);
    }
    @Nested
    class testEqualsMethode {
        @Test
        public void toEqualsArmy() {
            Unit unit = new CavalryUnit("Test Cavalry", 1);
            Unit unit2 = new CavalryUnit("Test Cavalry", 1);
            Army army = new Army("Test");
            army.add(unit);
            army.add(unit2);

            Army army2 = new Army("Test");
            army2.add(unit);
            army2.add(unit2);

            assertTrue(army.equals(army2));
        }
        @Test
        public void armyHasDifferentNamesButSameUnits(){
            Army army = new Army("Test");
            army.add(new CavalryUnit("Test Cavalry", 1));
            Army army2 = new Army("Test2");
            army2.add(new CavalryUnit("Test Cavalry", 1));
            assertFalse(army.equals(army2));
        }
        @Test
        public void armyHasSameNamesButDifferentUnit(){
            Army army = new Army("Test");
            army.add(new CavalryUnit("Test Cavalry2", 1));
            Army army2 = new Army("Test");
            army2.add(new CavalryUnit("Test Cavalry", 1));
            assertFalse(army.equals(army2));
        }
    }
    @Test
    public void testGetInfantryUnit(){
        Army army = new Army("Test");
        Unit inf1=new InfantryUnit("Unit1",1);
        Unit inf2=new InfantryUnit("Unit2",1);

        army.add(inf1);
        army.add(inf2);

        army.add(new CavalryUnit("Unit1",1));
        assertEquals(army.getInfantryUnits(), List.of(inf1, inf2));
        assertEquals(army.getInfantryUnits().size(), 2);
    }
    @Test
    public void testGetInfantryUnitReturnNull(){
        Army army = new Army("Test");

        army.add(new CavalryUnit("Unit1",1));
        assertEquals(army.getInfantryUnits(), new ArrayList<>());
        assertEquals(army.getInfantryUnits().size(), 0);
    }

    @Test
    public void testGetCavalryAndCommander(){
        Army army = new Army("Test");
        Unit inf1=new CommanderUnit("Unit1",1);
        Unit inf2=new CommanderUnit("Unit2",1);

        army.add(inf1);
        army.add(inf2);

        army.add(new CavalryUnit("Unit1",1));

        assertEquals(army.getCommanderUnits(), List.of(inf1, inf2));
        assertEquals(army.getCommanderUnits().size(), 2);

        assertEquals(army.getCavalryUnits(), List.of(new CavalryUnit("Unit1",1)));
        assertEquals(army.getCavalryUnits().size(),1);
    }
    @Test
    public void testReadCsvFileNotFound(){
        try {
            Army army = Army.readCsvFile("src\\main\\resources\\fileNotFound.csv");
            assertNull(army);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testReadCsvFileNotValidSeparators(){
        try {
            Army army = Army.readCsvFile("src\\main\\resources\\notvalidseparators.csv");
        } catch (Exception e) {
            assertEquals(e.getMessage(),"Wrong format");
        }
    }
    @Test
    public void testReadCsvFileWrongFormat(){
        try {
            Army army = Army.readCsvFile("src\\main\\resources\\wrongformat.csv");
        } catch (Exception e) {
            assertEquals(e.getMessage(),"Wrong format");
        }
    }

    @Test
    public void armyTakeUnitsFromCsvAndAfterWriteInFile(){
        Army army = null;
        try {
            army = Army.readCsvFile("src\\main\\resources\\database.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
        army.writeCsvFile("src\\main\\resources\\databasewrite.csv");



        File fileDatabase = new File("src\\main\\resources\\database.csv");
        File fileDatabaseWrite = new File("src\\main\\resources\\databasewrite.csv");


        Scanner scanner1 = null;
        try {
            scanner1 = new Scanner(fileDatabase);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Scanner scanner2 = null;
        try {
            scanner2 = new Scanner(fileDatabaseWrite);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (scanner1.hasNext()){
            String lineInFile1 = scanner1.nextLine();
            String lineInFile2 = scanner2.nextLine();
            assertEquals(lineInFile1,lineInFile2);
        }
        scanner1.close();
        scanner2.close();
    }

}