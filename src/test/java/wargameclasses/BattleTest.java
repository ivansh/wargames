package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {
    @Nested
    class testSimulate {

        @Test
        public void armyOneHasNoUnits() {
            Army army1 = new Army("Army1");
            Army army2 = new Army("Army2");
            army2.add(new RangedUnit("Unit", 100));
            Battle testBattle = new Battle(army1, army2);
            try {
                assertEquals(testBattle.simulate(), army2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Test
        public void armyTwoHasNoUnits() {
            Army army1 = new Army("Army1");
            Army army2 = new Army("Army2");
            army1.add(new RangedUnit("Unit", 100));
            Battle testBattle = new Battle(army1, army2);
            try {
                assertEquals(testBattle.simulate(), army1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Test
        public void armiesAreEmpty() {
            Army army1 = new Army("Army1");
            Army army2 = new Army("Army2");
            Battle testBattle = new Battle(army1, army2);
            try {
                testBattle.simulate();
            }catch (Exception e){
                assertEquals(e.getMessage(),"Armies are empty");
            }
        }

        @Test
        public void armiesAreEquals() {
            Army army1 = new Army("Army");
            Army army2 = new Army("Army");
            army1.add(new RangedUnit("Unit", 100));
            army2.add(new RangedUnit("Unit", 100));
            Battle testBattle = new Battle(army1, army2);
            try {
                testBattle.simulate();
            }catch (Exception e) {
                assertEquals(e.getMessage(), "Armies are equal");
            }
        }

        @Test
        public void armiesAreEqualsByNameButHasDifferentCollection() {
            Army army1 = new Army("Army");
            Army army2 = new Army("Army");
            army1.add(new RangedUnit("Unit", 100));
            army2.add(new CavalryUnit("Unit", 100));
            Battle testBattle = new Battle(army1, army2);
            try {
                assertEquals(testBattle.simulate().getAllUnits().size(), 1);// size after simulation = 1 that means that simulation was activated
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Test
        public void armiesAreEqualsByCollectionButHasDifferentNames() {
            Army army1 = new Army("Army");
            Army army2 = new Army("Army 2");
            army1.add(new RangedUnit("Unit", 100));
            army2.add(new RangedUnit("Unit", 100));
            Battle testBattle = new Battle(army1, army2);
            try {
                assertEquals(testBattle.simulate().getAllUnits().size(), 1);// size after simulation = 1 that means that simulation was activated
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Test
        //if units are the same, it means that army which will first attack will win (it is always first army)
        public void armyOneIsTheWinner() {
            Army army1 = new Army("Army");
            Army army2 = new Army("Army 2");
            army1.add(new RangedUnit("Unit", 100));
            army2.add(new RangedUnit("Unit2", 100));
            Battle testBattle = new Battle(army1, army2);
            try {
                assertEquals(testBattle.simulate(), army1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Test
        //army 2 have more powerful collection
        public void armyToIsTheWinner() {
            Army army1 = new Army("Army");
            Army army2 = new Army("Army 2");
            army1.add(new RangedUnit("Unit", 100));
            army2.add(new CavalryUnit("Unit2", 100));
            army2.add(new RangedUnit("Unit2", 100));

            Battle testBattle = new Battle(army1, army2);
            try {
                assertEquals(testBattle.simulate(), army2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }@Nested
    class testOfTerrainImplementation {
        Army army1 = new Army("Test1");
        Army army2 = new Army("Test2");

        @Test
        public void testConstructor(){
            army1.add(new InfantryUnit("1",1));
            army1.add(new RangedUnit("1",1));
            army2.add(new CavalryUnit("1",1));
            Battle battle = new Battle(army1,army2,TerrainType.FOREST);
            assertEquals(army1.getInfantryUnits().get(0).getAttackBonus(),4);
            assertEquals(army1.getInfantryUnits().get(0).getResistBonus(),3);
            assertEquals(army1.getRangedUnits().get(0).getAttackBonus(),2);

            assertEquals(army2.getCavalryUnits().get(0).getResistBonus(),0);
        }
        @Test
        public void testSetTerrain(){
            army1.add(new InfantryUnit("1",1));
            army2.add(new CavalryUnit("1",1));

            Battle battle= new Battle(army1,army2);

            battle.setTerrainInBattle(TerrainType.FOREST);

            assertEquals(army1.getInfantryUnits().get(0).getAttackBonus(),4);
            assertEquals(army1.getInfantryUnits().get(0).getResistBonus(),3);

            assertEquals(army2.getCavalryUnits().get(0).getResistBonus(),0);

            battle.setTerrainInBattle(TerrainType.PLAINS);

            assertEquals(army1.getInfantryUnits().get(0).getAttackBonus(),2);
            assertEquals(army1.getInfantryUnits().get(0).getResistBonus(),1);

            assertEquals(army2.getCavalryUnits().get(0).getAttackBonus(),8);



        }
    }
}

