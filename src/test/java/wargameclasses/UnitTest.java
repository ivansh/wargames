package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTest {

    /**
     * Test if our Constructor-Method throws exception
     */
    @Test
    public void healthIsNegative() {
        try {
            Unit unit = new Unit("Army", -1, 23, 23) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
        } catch (IllegalArgumentException exception) {
            assertEquals(exception.getMessage(), "Health can`t be negative");
        }

    }

    /**
     * Tests for our main method in which we construct our app
     */
    @Nested
    class testAttack {

        @Test
        public void attackOpponentWhichWillStayOneHP() {
            Unit unit = new Unit("Unit", 10, 20, 10) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            Unit opponent = new Unit("Opponent", 21, 0, 0) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            unit.attack(opponent);
            assertEquals(opponent.getHealth(), 1);

        }

        @Test
        public void attackOpponentWhichWillStayNullHP() {
            Unit unit = new Unit("Unit", 10, 20, 10) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            Unit opponent = new Unit("Opponent", 20, 0, 0) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            unit.attack(opponent);
            assertEquals(opponent.getHealth(), 0);
        }

        @Test
        public void attackOpponentWhichWillStayNegativeHP() {
            Unit unit = new Unit("Unit", 10, 20, 10) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            Unit opponent = new Unit("Opponent", 19, 0, 0) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            unit.attack(opponent);
            assertEquals(opponent.getHealth(), -1);
        }

        @Test
        public void attackOpponentWhichWillHaveResistBonus() {
            Unit unit = new Unit("Unit", 10, 20, 10) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            Unit opponent = new Unit("Opponent", 19, 0, 0) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 1;
                }
            };
            unit.attack(opponent);
            assertEquals(opponent.getHealth(), 0);
        }

        @Test
        public void attackOfUnitWhichHasAttackBonus() {
            Unit unit = new Unit("Unit", 10, 20, 10) {
                @Override
                public int getAttackBonus() {
                    return 1;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            Unit opponent = new Unit("Opponent", 19, 0, 0) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            unit.attack(opponent);
            assertEquals(opponent.getHealth(), -2);
        }

        @Test
        public void testOfAttackWithModifications() {
            Unit unit = new Unit("Unit", 10, 20, 10) {
                @Override
                public int getAttackBonus() {
                    return 10;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            Unit opponent = new Unit("Opponent", 10, 0, 10) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 10;
                }
            };
            unit.attack(opponent);
            assertEquals(opponent.getHealth(), 0);
        }
    }
    @Test
    public void testEqualMethodReturnTrue(){
        Unit unit = new Unit("Unit", 10, 0, 10) {
            @Override
            public int getAttackBonus() {
                return 0;
            }

            @Override
            public int getResistBonus() {
                return 0;
            }
        };
        Unit opponent = new Unit("Unit", 10, 0, 10) {
            @Override
            public int getAttackBonus() {
                return 0;
            }

            @Override
            public int getResistBonus() {
                return 0;
            }
        };
        assertTrue(unit.equals(opponent));
    }
    @Nested
    class testEquals {
        @Test
        public void testEqualMethodReturnFalse() {
            Unit unit = new Unit("Unit", 10, 20, 10) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            Unit opponent = new Unit("Opponent", 10, 0, 10) {
                @Override
                public int getAttackBonus() {
                    return 0;
                }

                @Override
                public int getResistBonus() {
                    return 0;
                }
            };
            assertFalse(unit.equals(opponent));
        }
    }
}
