package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class UnitFactoryTest {
    @Nested
    class testGetUnitMethode{
        @Test
        public void getInfantryUnit(){
            Unit unit = UnitFactory.getUnit(UnitType.INFANTRY,"Test",1);
            Unit sameUnitButWithUsualConstructor = new InfantryUnit("Test",1);
            assertTrue(unit.equals(sameUnitButWithUsualConstructor));
        }
        @Test
        public void getRangedUnit(){
            Unit unit = UnitFactory.getUnit(UnitType.RANGED,"Test",1);
            Unit sameUnitButWithUsualConstructor = new RangedUnit("Test",1);
            Unit unit1 = new InfantryUnit("Test",1);
            assertTrue(unit.equals(sameUnitButWithUsualConstructor));
            assertFalse(unit.equals(unit1));
        }
        @Test
        public void getCavalryUnit(){
            Unit unit = UnitFactory.getUnit(UnitType.CAVALRY,"Test",1);
            Unit sameUnitButWithUsualConstructor = new CavalryUnit("Test",1);
            Unit unit1 = new InfantryUnit("Test",1);
            assertTrue(unit.equals(sameUnitButWithUsualConstructor));
            assertFalse(unit.equals(unit1));
        }
        @Test
        public void getCommanderUnit(){
            Unit unit = UnitFactory.getUnit(UnitType.COMMANDER,"Test",1);
            Unit sameUnitButWithUsualConstructor = new CommanderUnit("Test",1);
            Unit unit1 = UnitFactory.getUnit(UnitType.INFANTRY,"Test",1);
            assertTrue(unit.equals(sameUnitButWithUsualConstructor));
            assertFalse(unit.equals(unit1));
        }
        @Test
        public void getUnitThrowsException(){
            try{
                Unit unit = UnitFactory.getUnit(UnitType.INFANTRY,"Test",-1);
            }catch (IllegalArgumentException exception){
                assertEquals(exception.getMessage(),"Health can`t be negative");
            }
        }
    }
    @Nested
    class testGetUnitList{
        @Test
        public void getUnitListTrowsException(){
            try {
                ArrayList<Unit> units = UnitFactory.getUnitsList(UnitType.INFANTRY,"Test",1,0);
            }catch (IllegalArgumentException exception){
                assertEquals(exception.getMessage(),"Enter the valid number of units in list(more than 0)");
            }
        }
        @Test
        public void getInfantryUnitsListWith1Instance(){
            ArrayList<Unit> units = UnitFactory.getUnitsList(UnitType.INFANTRY,"Test",1,1);
            assertEquals(new InfantryUnit("Test",1),units.get(0)); //f.ex last
            assertEquals(units.size(),1);
        }
        @Test
        public void armyGeneratedBy2DifferentWay(){
            Army army1 = new Army("Test Army");
            Army army2 = new Army("Test Army");
            //4 infantry units in
            army1.addAll(UnitFactory.getUnitsList(UnitType.INFANTRY,"Test",1,4));

            army2.add(new InfantryUnit("Test",1));
            army2.add(new InfantryUnit("Test",1));
            army2.add(new InfantryUnit("Test",1));
            army2.add(new InfantryUnit("Test",1));

            assertTrue(army1.equals(army2));

        }

    }

}