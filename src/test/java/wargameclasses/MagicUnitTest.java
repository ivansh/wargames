package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MagicUnitTest {

        @Test
        public void healthIsNegativeInSimplifiedConstructor(){
            try {
                Unit unit = new MagicUnit("Army",-12);
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Health can`t be negative");
            }
        }
        @Nested
        class testMAgicUnitOnDifferentTerrains{
            MagicUnit wizard = new MagicUnit("Test",1);
            @Test
            public void testMagicUnitInForest(){
                wizard.setTerrainType(TerrainType.FOREST);
                int bonus = wizard.getAttackBonus();
                boolean condition = ((bonus==10)||(bonus==6));
                assertTrue(condition);
               // assertEquals(wizard.getAttackBonus(),10);

            }
            @Test
            public void testMagicUnitInHills(){
                wizard.setTerrainType(TerrainType.HILL);
                int bonus = wizard.getAttackBonus();
                boolean condition = ((bonus==7)||(bonus==9));
                assertTrue(condition);
            }
            @Test
            public void testMagicUnitInPlains(){
                wizard.setTerrainType(TerrainType.PLAINS);
                int bonus = wizard.getAttackBonus();
                boolean condition = ((bonus==5) ||(bonus==4));
                assertTrue(condition);
            }
            @Test
            public void testMagicUnitWithoutTerrain(){
                wizard.setTerrainType(TerrainType.NOTERRAIN);
                int bonus = wizard.getAttackBonus();
                boolean condititon = ((bonus==7) ||(bonus==6));
                assertTrue(condititon);
            }
        }
        @Test
        public void testDefense(){
            RangedUnit attacker = new RangedUnit("12",12);
            MagicUnit wizard = new MagicUnit("Test",100);
            assertEquals(wizard.getResistBonus(),10);
            attacker.attack(wizard);
            assertEquals(wizard.getResistBonus(),0);
            attacker.attack(wizard);
            assertEquals(wizard.getResistBonus(),0);

        }

    }
