package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {

        @Test
        public void healthIsNegativeInSimplifiedConstructor(){
            try {
                Unit unit = new InfantryUnit("Army",-12);
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Health can`t be negative");
            }
        }
        @Nested
        class testInfantryUnitOnDifferentTerrains{
            Unit inf = new InfantryUnit("Test",1);
            @Test
            public void testRangedUnitInHill(){
                inf.setTerrainType(TerrainType.HILL);
                assertEquals(inf.getAttackBonus(),2);
                assertEquals(inf.getResistBonus(),1);
            }
            @Test
            public void testRangedUnitInForest(){
                inf.setTerrainType(TerrainType.FOREST);
                assertEquals(inf.getAttackBonus(),4);
                assertEquals(inf.getResistBonus(),3);
            }
            @Test
            public void testRangedUnitInPlains(){
                inf.setTerrainType(TerrainType.PLAINS);
                assertEquals(inf.getAttackBonus(),2);
                assertEquals(inf.getResistBonus(),1);
            }
            @Test
            public void testRangedUnitWithoutTerrain(){
                inf.setTerrainType(TerrainType.NOTERRAIN);
                assertEquals(inf.getAttackBonus(),2);
                assertEquals(inf.getResistBonus(),1);
            }
        }

    }
