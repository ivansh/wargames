package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CavalryUnitTest {

        @Test
        public void healthIsNegativeInSimplifiedConstructor() {
            try {
                Unit unit = new CavalryUnit("Army", -12);
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Health can`t be negative");
            }
        }


    @Nested
    class testOfSpecialMechanicsOfCavalryUnit {
        private Unit attacker = new CavalryUnit("Test", 100);
        private Unit defender = new RangedUnit("Test", 100);

        @Test
        public void inFirstAttackCavalryUnitGet6AdditionalAttack() {
            assertEquals(attacker.getAttackBonus(), 6);
        }

        @Test
        public void AfterFirstAttackCavalryUnitGet2AdditionalAttack() {
            attacker.attack(defender);
            assertEquals(attacker.getAttackBonus(), 2);
        }

        @Test
        public void TestOfTwoSpecialMechanicsInOurGame() {
            attacker.attack(defender); // basic hp 100 - attack (20+6) + armor (8+6)
            assertEquals(defender.getHealth(), 88);
            attacker.attack(defender); // 88 - attack (20+2) + armor (8+4)
            assertEquals(defender.getHealth(), 78);
            attacker.attack(defender); // 78 - attack (20+2) + armor (8+2)
            assertEquals(defender.getHealth(), 66);





        }


    }
    @Nested
    class testCavalryUnitOnDifferentTerrains{
        Unit cav = new CavalryUnit("Test",1);
        @Test
        public void testRangedUnitInHill(){
            cav.setTerrainType(TerrainType.HILL);
            assertEquals(cav.getAttackBonus(),6);
            assertEquals(cav.getResistBonus(),1);
        }
        @Test
        public void testRangedUnitInForest(){
            cav.setTerrainType(TerrainType.FOREST);
            assertEquals(cav.getAttackBonus(),6);
            assertEquals(cav.getResistBonus(),0);
        }
        @Test
        public void testRangedUnitInPlains(){
            cav.setTerrainType(TerrainType.PLAINS);
            assertEquals(cav.getAttackBonus(),8);
            assertEquals(cav.getResistBonus(),1);
        }
        @Test
        public void testRangedUnitWithoutTerrain(){
            cav.setTerrainType(TerrainType.NOTERRAIN);
            assertEquals(cav.getAttackBonus(),6);
            assertEquals(cav.getResistBonus(),1);
        }
    }@Test
    public void testSpecialMechanicsInPlain(){
        Unit cav = new CavalryUnit("Test",1);
        Unit cav2 = new CavalryUnit("Test",1);
        cav.setTerrainType(TerrainType.PLAINS);
        assertEquals(cav.getAttackBonus(),8);
        cav.attack(cav2);
        assertEquals(cav.getAttackBonus(),4);
        cav.setTerrainType(TerrainType.NOTERRAIN);
        assertEquals(cav.getAttackBonus(),2);
    }
}