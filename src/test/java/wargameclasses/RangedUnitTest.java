package wargameclasses;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {

        @Test
        public void healthIsNegativeInSimplifiedConstructor(){
            try {
                Unit unit = new RangedUnit("Army",-12);
            } catch (IllegalArgumentException exception) {
                assertEquals(exception.getMessage(), "Health can`t be negative");
            }
        }

    @Nested
    class testOfSpecialMechanics {

        private Unit inf=new InfantryUnit("1",1);//unit which will attack our RangedUnit
        private Unit rang= new RangedUnit("test",100); // test Unit which will be attacked

        @Test
        public void firstAttackOnRangedReturn6ExtraArmor(){
            assertEquals(rang.getResistBonus(),6);
        }
        @Test
        public void rangedUnitBeingAttackedSecondTimesReturn4ExtraArmor(){
            inf.attack(rang);
            assertEquals(rang.getResistBonus(),4);
        }
        @Test
        public void rangedUnitBeingAttackedThirdTimesReturn2ExtraArmor(){
            inf.attack(rang);
            inf.attack(rang);
            assertEquals(rang.getResistBonus(),2);
        }
        @Test
        public void checkIfNewSetHpWorkCorrectly(){
            Unit rang2= new RangedUnit("Test2",15);
            inf.attack(rang2); // InfantryUnit has 15 basic attack+ 2 additional; and it will attack rang2 which has 8 + 6 armor=basic hp-3hp
            assertEquals(rang2.getHealth(),12);
            inf.attack(rang2); // InfantryUnit has 15 basic attack+ 2 additional; and it will attack rang2 which has 8 + 4 armor=basic hp-8hp
            assertEquals(rang2.getHealth(),7);
            inf.attack(rang2); // InfantryUnit has 15 basic attack+ 2 additional; and it will attack rang2 which has 8 + 2 armor=rang2 should be dead
            assertEquals(rang2.getHealth(),0);


        }
    }
    @Nested
    class testRangedUnitOnDifferentTerrains{
        Unit ranged = new RangedUnit("Test",1);
        @Test
        public void testRangedUnitInHill(){
            ranged.setTerrainType(TerrainType.HILL);
            assertEquals(ranged.getAttackBonus(),6);
            assertEquals(ranged.getResistBonus(),6);
        }
        @Test
        public void testRangedUnitInForest(){
            ranged.setTerrainType(TerrainType.FOREST);
            assertEquals(ranged.getAttackBonus(),2);
            assertEquals(ranged.getResistBonus(),6);
        }
        @Test
        public void testRangedUnitInPlains(){
            ranged.setTerrainType(TerrainType.PLAINS);
            assertEquals(ranged.getAttackBonus(),3);
            assertEquals(ranged.getResistBonus(),6);
        }
        @Test
        public void testRangedUnitWithoutTerrain(){
            ranged.setTerrainType(TerrainType.NOTERRAIN);
            assertEquals(ranged.getAttackBonus(),3);
            assertEquals(ranged.getResistBonus(),6);
        }
    }

}